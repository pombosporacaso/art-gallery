///////////////////////////////////////////////////////////
//  Owner.h
//  Implementation of the Class Owner
//  Created on:      06-Nov-2013 1:03:13 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Owner_H__INCLUDED)
#define Owner_H__INCLUDED



#ifndef iostream
#include <iostream>
#endif

#ifndef fstream
#include <fstream>
#endif // !fstream

#ifndef vector
#include <vector>
#endif

#ifndef string
#include <string>
#endif 

enum owner_t
{
	Artist_o,
	Client_o,
	Gallery_o
};


class Piece;

class Owner
{

public:

	/**
	* Constructor that creates a default owner
	*
	*/
	Owner();

	/**
	* Destructor that eliminates the space occupied by an owner
	*
	*/
	virtual ~Owner();

	/**
	* Pure function that will "force" each of the sub classes to implement
	* their own way of initializing themselves
	*
	*@param filename which contains the saved data of the object
	*@return returns true or false whether the class was succesfully intialized
	*/
	virtual bool Initialize(const std::string& filename) = 0;

	/**
	* Pure function that will "force" each of the sub classes to implement
	* their own way of saving their data
	*
	*/
	virtual void Shutdown() = 0;

	/**
	* Function capable of adding a piece to the vector of owned pieces
	*
	*@param receives the ID of the piece to be added
	*@return returns true or false whether the piece was succesfully added or not
	*/
	bool AddOwnedPiece(unsigned int ID);

	bool BuyPiece(unsigned int ID, int price);

	/**
	* Pure function that will "force" each of the sub classes to implement
	* their own way of creating expositions
	*
	*@return returns true or false whether the exposition was succesfully created
	*/
	virtual bool CreateExposition() = 0;

	/**
	*Function that returns the name of the owner
	*
	*@return returns a string with the name of the owner
	*/
	const std::string& GetName() const;

	/**
	* Pure function to identify the type of owner
	*
	*@return an enum type owner_t
	*/
	virtual owner_t GetType() const = 0;

	const std::string& GetPassword() const;

	/**
	*Function that returns the vector of owned pieces
	*
	*@return returns a vector with the pieces of each owner
	*/
	const std::vector<Piece*>& GetOwnedPieces() const;

	/**
	* Function capable of removing a piece from the vector of owned pieces
	*
	*@param receives the ID of the piece to be removed
	*@return returns true or false whether the piece was succesfully removed or not
	*/
	bool RemoveOwnedPiece(unsigned int ID);

	/**
	* Function in charge of changing the name of the owner
	*
	*@param receives the owners' new name
	*/
	void SetName(const std::string& name);

	/**
	* Function in charge of changing a piece within the vector of owned pieces
	*
	*@param receives the index of the piece to be changed
	*@param receives the new piece
	*@return returns true or false whether the piece was succesfully changed or not
	*/
	bool SetOwnedPiece(unsigned int index, Piece* piece);

	void SetPassword(const std::string& pass);

	int GetTransactions() const;

	void SetTransactions(int trans);

	void IncTransactions();

	void IntAcumulatedValue(int value);

	int GetAcumulatedValue() const;

	void SetAcumulatedValue(int value);

	std::string GetAdress() const;

	void SetAdress(const std::string& adr);

	int GetInactiveTime() const;

	void SetInactiveTime(int time);

	bool IsClientActive(int period);

	/**
	* Function that overloads the operator == to be able to compare different owners
	*
	*@param receives the an owner to compare with
	*@return returns true or false whether the owners are or not the same
	*/
	bool operator==(const Owner& owner) const;

	bool operator<(const Owner& owner);

private:
	std::vector<Piece*> mOwnedPieces;
	std::string mName;
	std::string mPassword;
	std::string mAdress;
	int mInactiveTime;

	int mTransactions;
	int mAcumValue;
};


std::ostream& operator<<(std::ostream& os, const Owner& owner);
std::ofstream& operator<< (std::ofstream& os, const Owner& owner);
std::ifstream& operator>> (std::ifstream& is, Owner& owner);

#endif // !defined(Owner_H__INCLUDED)
