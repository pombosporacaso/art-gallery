///////////////////////////////////////////////////////////
//  Painting.h
//  Implementation of the Class Painting
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Painting_H__INCLUDED)
#define Painting_H__INCLUDED

#include "Piece.h"

class Painting : public Piece
{

public:
	/**
	* Constructor that creates a default Painting
	*
	*/
	Painting();

	/**
	* Destructor that eliminates the space occupied by an Painting
	*
	*/
	~Painting();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*@return returns returns true or false whether the class was succesfully intialized
	*/
	bool Initialize(const std::string& filename);

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	void Shutdown();

	/**
	* Function that tells us what type of piece we're dealing with
	*
	*@return returns a string "Painting", indicating we're dealing with an Painting
	*/
	std::string GetType() const;

	/**
	* Pure virtual function that returns the id of the piece depending on it's type
	*
	*@return returns the integer corresponding to the piece's type ID
	*/
	unsigned int GetTypeID() const;

private:
	unsigned int mPaintingID;
	static unsigned int msNumberPaintings;
};
#endif // !defined(Painting_H__INCLUDED)
