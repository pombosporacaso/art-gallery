///////////////////////////////////////////////////////////
//  Painting.cpp
//  Implementation of the Class Painting
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Painting.h"
#include "Exceptions.h"
#include "IGallery.h"

using namespace std;

unsigned int Painting::msNumberPaintings = 0;

Painting::Painting()
{
	msNumberPaintings++;
	mPaintingID = msNumberPaintings;
}
Painting::~Painting()
{
	msNumberPaintings--;
}

bool Painting::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Painting::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);
	IGallery::AddFilename(IGallery::mPaintingsFilename, filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

string Painting::GetType() const
{
	return "Painting";
}
unsigned int Painting::GetTypeID() const
{
	return mPaintingID;
}