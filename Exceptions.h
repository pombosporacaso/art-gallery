#if !defined(Exceptions_H__INCLUDED)
#define Exceptions_H__INCLUDED

#ifndef string
#include <string>
#endif

#ifndef ostream
#include <ostream>
#endif

class EGalleryException
{
public:
	EGalleryException();

	virtual std::string GetReason() const = 0;
	friend std::ostream& operator<<(std::ostream& os, const EGalleryException& e);
};

class ESaveException : public EGalleryException
{
public:
	ESaveException(const std::string filename);

	std::string GetReason() const;

private:
	std::string mFilename;
};

class EWrongID : public EGalleryException
{
public:
	EWrongID();

	std::string GetReason() const;
};

class EWrongUsername : public EGalleryException
{
public:
	EWrongUsername(bool isUsername);

	std::string GetReason() const;

private:
	bool mIsUsername;
};

class EEmpty : public EGalleryException
{
public:
	EEmpty();

	std::string GetReason() const;
};
class EDefault : public EGalleryException
{
public:
	EDefault(const std::string &reason);

	std::string GetReason() const;

private:
	std::string mReason;
};

#endif // !Exceptions_H__INCLUDED