///////////////////////////////////////////////////////////
//  Client.h
//  Implementation of the Class Client
//  Created on:      06-Nov-2013 1:03:11 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Client_H__INCLUDED)
#define Client_H__INCLUDED

#include "Person.h"

class Client : public Person
{

public:

	/**
	* Constructor that creates a default client
	*
	*/
	Client();

	/**
	* Destructor that eliminates the space occupied by an client
	*
	*/
	virtual ~Client();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*@return returns true or false whether the class was succesfully intialized
	*/
	bool Initialize(const std::string& filename);

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	void Shutdown();

	/**
	* Function returns the ID of the client
	*
	*@return returns the ID of the client
	*/
	unsigned int GetID() const;

	/**
	* Function that tells us what type of owner we're dealing with
	*
	*@return the owner_t Client_o, indicating we're dealing with a client
	*/
	owner_t GetType() const;

	void GetFilename(std::string& filename) const;


private:
	unsigned int mClientID;
	static unsigned int msNumberClients;

	bool SaveData(const std::string& filename) const;
	bool LoadData(const std::string& filename);
};


#endif // !defined(Client_H__INCLUDED)
