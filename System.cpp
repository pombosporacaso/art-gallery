///////////////////////////////////////////////////////////
//  System.cpp
//  Implementation of the Class System
//  Created on:      06-Nov-2013 1:03:15 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "System.h"
#include "Owner.h"
//#include "IGallery.h"
#include "IOGallery.h"
#include "Artist.h"
#include "Client.h"
#include "Gallery.h"
#include "Piece.h"
#include "Transaction.h"
#include "Exceptions.h"
#include "Photo.h"
#include "Painting.h"
#include "Handicraft.h"
#include "Sculpture.h"
#include "Exposition.h"

#ifndef iostream
#include <iostream>
#endif // !iostream

#ifndef iomanip
#include <iomanip>
#endif // !iomanip

#ifndef conio
#include <conio.h>
#endif // !conio.h

using namespace std;

System::System()
{
	mIGallery = 0;
}
System::~System()
{

}

bool System::Initialize()
{
	bool result;

	mIGallery = new IGallery();
	if (!mIGallery)
		return false;

	result = mIGallery->Initalize();
	if (!result)
		return false;

	return true;
}
void System::Run()
{
	// Preview
	mCurrentMenu = Preview_m;

	while (true)
	{
		try
		{
			switch (mCurrentMenu)
			{
			case Preview_m:		Preview();		break;

			case LoginMenu_m:	LoginMenu();	break;
			case MainMenu_m:	MainMenu();		break;
			case SearchMenu_m:	SearchMenu();	break;
			case ArtistMenu_m:	ArtistMenu();	break;
			case ArtistMenu2_m: ArtistMenuNP(); break;
			case ClientMenu_m:	ClientMenu();	break;
			case AdminMenu_m:	AdminMenu();	break;

			case Exit_m: return;
			}
		}
		catch (const EGalleryException& e)
		{
			cout << endl << e.GetReason();
			_getch();
		}

		system("cls");
	}

	return;
}
void System::Shutdown()
{
	if (mIGallery)
	{
		mIGallery->Shutdown();
		delete mIGallery;
		mIGallery = 0;
	}
}

void System::Preview()
{
	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl << setw(25) << " " << "AEDA Project" << endl << endl << setw(25) << " " << "Art Gallery" << endl;

	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << setw(63) << " " << "Programmed by:" << endl << endl << setw(65) << " " << "Joao Maia\n" << setw(65) << " " << "Jose Oliveira\n" << setw(65) << " " << "Leonardo Faria";

	// Waiting for key press
	int key = _getch();

	// Login Menu
	mCurrentMenu = LoginMenu_m;
}
void System::LoginMenu()
{
	// Screen
	cout
		<< "Welcome to PombosPorAcasoTartarugas Gallery!" << endl
		<< "Are you a registered user?" << endl << endl
		<< "1. Login" << endl
		<< "2. Register" << endl
		<< "3. Quit" << endl;

	while (true)
	{
		// Waiting for key press
		char option = _getch();

		// Selecting options
		switch (option)
		{
		case '1':
			if (LoginUser())
				mCurrentMenu = MainMenu_m;
			return;
		case '2':
			if (RegisterUser())
				mCurrentMenu = MainMenu_m;
			return;
		case '3':
			mCurrentMenu = Exit_m;
			return;
		}
	}
}
bool System::LoginUser()
{
	system("cls");

	// User name input
	cout << "User: ";
	string user;
	if (!(*IGallery::mInput >> user))
		return false;

	// Password input
	cout << "Password: ";
	string password;
	if (!(IGallery::mInput->InputPassword(password)))
		return false;

	// Searching for the user
	Owner* owner = 0;
	if (IGallery::GetOwner(user, owner))
	{
		if (owner->GetPassword() == password)
		{
			vector<Piece*> temp = IGallery::GetPieces();
			for (unsigned int i = 0; i < temp.size(); i++)
			{
				if (temp[i]->GetOwner() == *owner)
					owner->AddOwnedPiece(temp[i]->GetID());
			}
			mpCurrentUser = owner;
			return true;
		}
	}
	else
		throw EWrongUsername(true);

	throw EWrongUsername(false);
}
bool System::RegisterUser()
{
	system("cls");

	cout << "\nAre you an Artist or a Client?\n";
	string type;
	if (!IGallery::mInput->InputOwner(type))
		return false;

	if (type == "Artist")
	{
		Artist* artist = 0;
		if (!IGallery::CreateArtist(artist))
			return false;
		mpCurrentUser = artist;
		return true;
	}
	else if (type == "Client")
	{
		Client* client = 0;
		if (!IGallery::CreateClient(client))
			return false;
		mpCurrentUser = client;
		return true;
	}

	return false;
}

void System::MainMenu()
{
	cout
		<< "1. Pieces" << endl
		<< "2. Expositions" << endl
		<< "3. Pieces for sale " << endl
		<< "4. Pieces in Storage " << endl
		<< "5. Transactions" << endl
		<< "6. Search Menu" << endl
		<< "7. Quit" << endl
		<< "\n\n\n9. Update Time" << endl;

	owner_t user;
	user = mpCurrentUser->GetType();
	switch (user)
	{
	case Artist_o:
		cout << endl << "A. Artist Menu" << endl;
		break;
	case Client_o:
		cout << endl << "C. Client Menu" << endl;
		break;
	case Gallery_o:
		cout << endl << "G. Gallery Menu" << endl;
		break;
	default:
		break;
	}

	while (true)
	{
		// Waiting for key press
		char option = _getch();

		// Selecting menu to show
		switch (option)
		{
		case '1':	ShowPieces();					return;
		case '2':	ShowExpositions();				return;
		case '3':	ShowPiecesForSale();			return;
		case '4':	ShowPiecesStorage();			return;
		case '5':	ShowExpositions();				return;
		case '6':	mCurrentMenu = SearchMenu_m;	return;
		case '7':	mCurrentMenu = Exit_m;			return;
		case '9':	UpTime();						return;
		case 'g':
			if (user == Gallery_o)
				mCurrentMenu = AdminMenu_m;
			return;
		case 'c':
			if (user == Client_o)
				mCurrentMenu = ClientMenu_m;
			return;
		case 'a':
			if (user == Artist_o)
				mCurrentMenu = ArtistMenu_m;
			return;
		}
	}
}
void System::AdminMenu()
{
	cout
		<< "1. Owned Pieces" << endl
		<< "2. Buy Piece" << endl
		<< "3. Sell Piece" << endl
		<< "4. Remove Piece from Sell List" << endl
		<< "5. Remove Piece from Storage" << endl
		<< "6. Create Exposition" << endl
		<< "7. Remove Artist" << endl
		<< "8. Remove Client" << endl
		<< "9. Quit" << endl;

	while (true)
	{
		// Waiting for key press
		char option = _getch();

		switch (option)
		{
		case '1': ShowOwnedPieces();			return;
		case '2': BuyPiece();					return;
		case '3': SellPiece();					return;
		case '4': RemovePieceForSale();			return;
		case '5': RemovePieceStorage();			return;
		case '6': CreateReservation();			return;
		case '7': RemoveArtist();				return;
		case '8': RemoveClient();				return;
		case '9': mCurrentMenu = MainMenu_m;	return;
		}
	}
}
void System::ArtistMenu()
{
	cout
		<< "1. Owned Pieces" << endl
		<< "2. Buy Piece" << endl
		<< "3. Sell Piece" << endl
		<< "4. Create Piece" << endl
		<< "5. Store Piece" << endl
		<< "6. Next Page" << endl
		<< "7. Quit" << endl;

	while (true)
	{
		// Waiting for key press
		char option = _getch();

		switch (option)
		{
		case '1': ShowOwnedPieces();				return;
		case '2': BuyPiece();						return;
		case '3': SellPiece();						return;
		case '4': CreatePiece();					return;
		case '5': StorePiece();						return;
		case '6': mCurrentMenu = ArtistMenu2_m;		return;
		case '7': mCurrentMenu = MainMenu_m;		return;
		}
	}
}
void System::ArtistMenuNP()
{
	cout
		<< "1. Create Exposition" << endl
		<< "2. Authorize Piece to be exposed" << endl
		<< "3. Unauthorize Piece from being exposed" << endl
		<< "4. Remove Piece" << endl
		<< "5. Previous Page" << endl
		<< "6. Quit" << endl;

	while (true)
	{
		// Waiting for key press
		char option = _getch();

		switch (option)
		{
		case '1': CreateReservation();			return;
		case '2': AuthorizePiece();				return;
		case '3': UnauthorizePiece();			return;
		case '4': RemovePiece();				return;
		case '5': mCurrentMenu = ArtistMenu_m;	return;
		case '6': mCurrentMenu = MainMenu_m;	return;
		}
	}
}
void System::ClientMenu()
{
	cout
		<< "1. Owned Pieces" << endl
		<< "2. Buy Piece" << endl
		<< "3. Sell Piece" << endl
		<< "4. Store Piece" << endl
		<< "5. Create Reservation" << endl
		<< "6. Authorize Piece to be exposed" << endl
		<< "7. Unauthorize Piece from being exposed" << endl
		<< "8. Remove Piece" << endl
		<< "9. Quit" << endl;

	while (true)
	{
		// Waiting for key press
		char option = _getch();

		switch (option)
		{
		case '1': ShowOwnedPieces();			return;
		case '2': BuyPiece();					return;
		case '3': CreatePiece();				return;
		case '4': StorePiece();					return;
		case '5': CreateReservation();			return;
		case '6': AuthorizePiece();				return;
		case '7': UnauthorizePiece();			return;
		case '8': RemovePiece();				return;
		case '9': mCurrentMenu = MainMenu_m;	return;
		}
	}
}

void System::UpTime()
{
	system("cls");
	int binactiveTime = IGallery::ts.tm_mday + IGallery::ts.tm_mday*IGallery::ts.tm_mon;
	
	IGallery::UpdateTime();

	int finactiveTime = IGallery::ts.tm_mday + IGallery::ts.tm_mday*IGallery::ts.tm_mon;

	int period = finactiveTime - binactiveTime;

	IGallery::SetPeriod(period);

	vector<Owner*> owners = IGallery::GetOwners();

	for (unsigned int i = 0; i < owners.size(); i++)
	{
		if (mpCurrentUser != owners[i])
			owners[i]->SetInactiveTime(IGallery::GetPeriod());
	}
	cout << "Time succesfully updated" << endl;

	_getch();
}
void System::ShowPieces()
{
	system("cls");

	vector <Piece*> pieces = IGallery::GetPieces();

	if (pieces.empty())
		throw EEmpty();


	for (unsigned int i = 0; i < pieces.size(); i++)
	{
		cout << *pieces[i] << endl;
		if ((i + 1) % 10 == 0)
			_getch();
	}

	_getch();
}
void System::ShowExpositions()
{
	system("cls");

	vector<Exposition*> exp = IGallery::GetExpositions();

	if (exp.empty())
		throw EEmpty();

	for (unsigned int i = 0; i < exp.size(); i++)
	{
		cout << *exp[i];
		if ((i + 1) % 10 == 0)
			_getch();
	}

	_getch();
}
void System::ShowPiecesForSale()
{
	system("cls");
	int p = 0;

	vector <Piece*> pieces = IGallery::GetPieces();

	if (pieces.empty())
		throw EEmpty();

	for (unsigned int i = 0; i < pieces.size(); i++)
	{
		if (pieces[i]->IsForSale())
		{
			p++;
			cout << *pieces[i];
			if ((i + 1) % 10 == 0)
				_getch();
		}
	}
	if (p == 0)
		throw EEmpty();

	_getch();
}
void System::ShowPiecesStorage()
{
	system("cls");

	Gallery* gal = 0;
	IGallery::GetGallery(gal);

	BST<SPiece> pieces = gal->GetPiecesStorage();

	if (pieces.isEmpty())
		throw EEmpty();

	int i = 0;
	BSTItrIn<SPiece> itr(pieces);
	while (!itr.isAtEnd())
	{
		SPiece sp;
		sp = itr.retrieve();
		cout << *(sp.piece);
		itr.advance();
		if ((i + 1) % 10 == 0)
			_getch();
		++i;
	}
	_getch();
}
bool System::ShowTransactions()
{
	system("cls");

	cout << "Piece ID: \t";
	unsigned int ID;

	if (!(*IGallery::mInput >> ID))
		return false;

	Piece* pPiece = 0;
	if (!IGallery::GetPiece(ID, pPiece))
		throw EWrongID();

	vector <Transaction> trans = pPiece->GetTransactions();

	for (unsigned int i = 0; i < trans.size(); i++)
		cout << trans[i];

	_getch();
	return true;
}
void System::ShowOwnedPieces()
{
	system("cls");

	vector <Piece*> pieces = mpCurrentUser->GetOwnedPieces();

	if (pieces.empty())
		throw EEmpty();

	for (unsigned int i = 0; i < pieces.size(); i++)
		cout << *pieces[i] << endl;

	_getch();
}

bool System::RemovePieceForSale()
{
	system("cls");

	cout << "ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Gallery* gallery = 0;
	IGallery::GetGallery(gallery);

	if (!gallery->RemovePieceForSale(ID))
		throw EWrongID();

	return true;
}
bool System::RemovePieceStorage()
{
	system("cls");

	cout << "ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Gallery* gallery = 0;
	IGallery::GetGallery(gallery);

	if (!gallery->RemovePieceStorage(ID))
		throw EWrongID();

	return true;
}
bool System::RemoveArtist()
{
	system("cls");

	cout << "Name: \t";
	string name;
	if (!(*IGallery::mInput >> name))
		return false;

	if (!IGallery::DeleteArtist(name))
		throw EDefault("Piece couldn't be found");

	return true;
}
bool System::RemoveClient()
{
	system("cls");

	cout << "Name: \t";
	string name;
	if (!(*IGallery::mInput >> name))
		return false;

	if (!IGallery::DeleteClient(name))
		throw EDefault("Piece couldn't be found");

	return true;
}

bool System::CreateReservation()
{
	system("cls");

	Reservation* res = 0;

	if (IGallery::CreateReservation(res, mpCurrentUser))
	{
		vector<Exposition*> exp = IGallery::GetExpositions();
		priority_queue<Reservation*> res = IGallery::GetReservations();
		queue<Reservation*> temp;
		Gallery* gal = 0;
		IGallery::GetGallery(gal);

		while (!res.empty())
		{
			Reservation* r = res.top();
			int space = 0;
			space = gal->AvailableSpace(r->GetDate());

			if (r->GetExposition()->GetSpace() < space)
			{
				Exposition* e = 0;
				e = r->GetExposition();
				exp.push_back(e);
				res.pop();
			}
			else
			{
				temp.push(r);
				res.pop();
			}
		}

		while (!temp.empty())
		{
			res.push(temp.front());
			temp.pop();
		}

		IGallery::SetExpositions(exp);
		IGallery::SetReservations(res);
		IGallery::SaveData();
		return true;
	}
	else
		return false;

}

bool System::BuyPiece()
{
	system("cls");

	cout << "Piece ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Piece* piece = 0;
	if (!IGallery::GetPiece(ID, piece))
		throw EWrongID();

	if (!piece->IsForSale())
		throw EDefault("Piece not available for sale");

	Owner* seller = &piece->GetOwner();
	vector<Piece*> temp = IGallery::GetPieces();
	for (unsigned int i = 0; i < temp.size(); i++)
	{
		if (temp[i]->GetOwner() == *seller)
			seller->AddOwnedPiece(temp[i]->GetID());
	}
	Owner* buyer = mpCurrentUser;

	if (seller == buyer)
		throw EDefault("You cannot buy your own pieces");

	Transaction trans;
	trans.SetBuyer(*buyer);
	trans.SetSeller(*seller);

	if (!(IGallery::CreateTransaction(trans)))
		return false;

	piece->AddTransaction(trans);
	piece->SetForSale(false);
	piece->SetExpAuthorized(false);

	if (!seller->RemoveOwnedPiece(ID))
		throw EWrongID();
	if (!buyer->AddOwnedPiece(ID))
		throw EWrongID();

	seller->SetInactiveTime(0);
	buyer->SetInactiveTime(0);
	buyer->IncTransactions();
	buyer->IntAcumulatedValue(trans.GetPrice());

	buyer->Shutdown();
	return true;
}
bool System::SellPiece()
{
	system("cls");

	cout << "ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Gallery* gallery = 0;
	IGallery::GetGallery(gallery);

	if (!gallery->AddPieceForSale(ID))
		throw EWrongID();

	return true;
}
bool System::CreatePiece()
{
	system("cls");

	cout << "Type of Piece: \t";
	string type;
	if (!(IGallery::mInput->InputTheme(type)))
		return false;

	cout << endl;
	if (type == "Paintings")
	{
		Painting* piece = 0;
		if (!(IGallery::CreatePaiting(piece)))
			return false;

		piece->SetAuthor(mpCurrentUser->GetName());
		piece->SetOwner(mpCurrentUser);

		if (!piece->GetOwner().AddOwnedPiece(piece->GetID()))
			throw EWrongID();

		piece->Shutdown();
		return true;
	}
	else if (type == "Sculptures")
	{
		Sculpture* piece = 0;
		if (!(IGallery::CreateSculpture(piece)))
			return false;

		piece->SetAuthor(mpCurrentUser->GetName());
		piece->SetOwner(mpCurrentUser);

		if (!piece->GetOwner().AddOwnedPiece(piece->GetID()))
			throw EWrongID();

		piece->Shutdown();
		return true;
	}
	else if (type == "Photos")
	{
		Photo* piece = 0;
		if (!IGallery::CreatePhoto(piece))
			return false;

		piece->SetAuthor(mpCurrentUser->GetName());
		piece->SetOwner(mpCurrentUser);

		if (!piece->GetOwner().AddOwnedPiece(piece->GetID()))
			throw EWrongID();

		piece->Shutdown();
		return true;
	}
	else if (type == "Handicrafts")
	{
		Handicraft* piece = 0;
		if (!IGallery::CreateHandicraft(piece))
			return false;

		piece->SetAuthor(mpCurrentUser->GetName());
		piece->SetOwner(mpCurrentUser);

		if (!piece->GetOwner().AddOwnedPiece(piece->GetID()))
			throw EWrongID();

		piece->Shutdown();
		return true;
	}

	return false;
}
bool System::StorePiece()
{
	system("cls");

	cout << "Piece ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Gallery* gallery = 0;
	IGallery::GetGallery(gallery);

	if (!gallery->AddPieceStorage(ID))
		throw EWrongID();

	return true;
}
bool System::AuthorizePiece()
{
	system("cls");

	cout << "Piece ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Piece* piece = 0;
	if (!IGallery::GetPiece(ID, piece))
		throw EWrongID();

	piece->SetExpAuthorized(true);
	return true;
}
bool System::UnauthorizePiece()
{
	system("cls");

	cout << "Piece ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Piece* piece = 0;
	if (!IGallery::GetPiece(ID, piece))
		throw EWrongID();

	piece->SetExpAuthorized(false);
	return true;
}
bool System::RemovePiece()
{
	system("cls");

	cout << "Piece ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	if (!mpCurrentUser->RemoveOwnedPiece(ID))
		throw EWrongID();

	return true;
}

void System::SearchMenu()
{
	system("cls");

	cout
		<< "1. Pieces" << endl
		//<< "2. Expositions" << endl
		//<< "3. Artists" << endl
		<< "4. Quit" << endl;

	while (true)
	{
		// Waiting for key press
		int option = _getch() - 0x30;

		switch (option)
		{
		case 1:		SearchPieces();				return;
			//case 2: system("cls"); SearchExpositions(); return;
			//case 3: system("cls"); SearchArtists(); return;
		case 4:		mCurrentMenu = MainMenu_m;	return;
		}
	}
	return;
}
void System::SearchPieces()
{
	system("cls");

	cout
		<< "Search by:" << endl << "\t"
		<< "1. ID" << endl << "\t"
		<< "2. Name" << endl << "\t"
		<< "3. Type" << endl << "\t"
		<< "4. Owner" << endl << "\t"
		<< "5. Author" << endl << "\t"
		<< "6. Quit" << endl;

	while (true)
	{
		// Waiting for key press
		char option = _getch();

		switch (option)
		{
		case '1':	SearchPiecesID();			return;
		case '2':	SearchPiecesName();			return;
		case '3':	SearchPiecesType();			return;
		case '4':	SearchPiecesOwner();		return;
		case '5':	SearchPiecesAuthor();		return;
		case '6':	mCurrentMenu = SearchMenu_m;	return;
		}
	}
	return;
}
bool System::SearchPiecesID()
{
	system("cls");

	cout << "Piece ID: \t";
	unsigned int ID;
	if (!(*IGallery::mInput >> ID))
		return false;

	Piece* piece = 0;
	if (!IGallery::GetPiece(ID, piece))
		throw EWrongID();

	cout << *piece;
	_getch();
	return true;
}
bool System::SearchPiecesName()
{
	system("cls");

	cout << "Piece Name: \t";
	string name;
	if (!(*IGallery::mInput >> name))
		return false;

	Piece* piece = 0;
	if (!IGallery::GetPiece(name, piece))
		throw EWrongID();

	cout << *piece;
	_getch();
	return true;
}
bool System::SearchPiecesType()
{
	system("cls");

	cout << "Piece Type: \t";

	string type;
	if (!IGallery::mInput->InputTheme(type))
		return false;

	vector<Piece*>::iterator it;
	vector<Piece*> gPieces = IGallery::GetPieces();
	if (gPieces.empty())
		throw EEmpty();

	for (it = gPieces.begin(); it != gPieces.end(); it++)
	{
		if ((*it)->GetType() == type)
		{
			cout << **it;
			_getch();
			break;
		}
	}

	return true;
}
bool System::SearchPiecesOwner()
{
	system("cls");

	cout << "Piece Owner: \t";

	string owner;
	if (!(*IGallery::mInput >> owner))
		return false;

	vector<Piece*>::iterator it;
	vector<Piece*> gPieces = IGallery::GetPieces();
	if (gPieces.empty())
		throw EEmpty();

	for (it = gPieces.begin(); it != gPieces.end(); it++)
	{
		if ((*it)->GetOwner().GetName() == owner)
		{
			cout << **it;
			_getch();
			break;
		}
	}

	return true;
}
bool System::SearchPiecesAuthor()
{
	system("cls");

	cout << "Piece Author: \t";

	string author;
	if (!(*IGallery::mInput >> author))
		return false;

	vector<Piece*>::iterator it;
	vector<Piece*> gPieces = IGallery::GetPieces();
	if (gPieces.empty())
		throw EEmpty();

	for (it = gPieces.begin(); it != gPieces.end(); it++)
	{
		if ((*it)->GetAuthor() == author)
		{
			cout << **it;
			_getch();
			break;
		}
	}

	return true;
}