///////////////////////////////////////////////////////////
//  Person.cpp
//  Implementation of the Class Person
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Person.h"
#include "Exposition.h"
#include "IGallery.h"
#include "Gallery.h"
#include "Piece.h"
#include"IOGallery.h"

using namespace std;

Person::Person()
{
}
Person::~Person()
{
}

bool Person::AuthExposition(Piece* piece)
{
	unsigned int id;
	cout << "\tID of Piece to authorize: \n\t";
	if (!(*IGallery::mInput >> id))
		return false;

	vector<Piece*> oPiece = Owner::GetOwnedPieces();

	for (unsigned int i = 0; i < oPiece.size(); i++)
	{
		if (oPiece[i] == piece)
		{
			oPiece[i]->SetExpAuthorized(true);
			return true;
		}
	}
	
	return false;
}
bool Person::CreateExposition()
{
	Exposition* exp = 0;

	return (IGallery::CreateExposition(exp, false));
}