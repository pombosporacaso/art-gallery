///////////////////////////////////////////////////////////
//  IGallery.h
//  Implementation of the Class IGallery
//  Created on:      06-Nov-2013 1:03:13 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(IGallery_H__INCLUDED)
#define IGallery_H__INCLUDED

#ifndef string
#include <string>
#endif

#ifndef vector
#include <vector>
#endif

#ifndef queue
#include <queue>
#endif

#ifndef unordered_set
#include <unordered_set>
#endif

#include "Client.h"
#include <time.h>

#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable: 4996)

class Artist;
//class Client;
class Person;
class Reservation;
class Gallery;
class Owner;
class Exposition;
class Photo;
class Painting;
class Handicraft;
class Sculpture;
class Piece;
class Transaction;
class Date;
class IOGallery;

struct HashClient
{
	bool operator()(const Client& cli1, const Client& cli2)
	{
		if (cli1.GetName() == cli2.GetName())
			return true;
		else
			return false;
	}

	int operator()(const Client& cli)
	{
		int v = 0;
		std::string name = cli.GetName();

		for (unsigned int i = 0; i < name.size(); i++)
			v = 37 * v + name[i];
		return v;
	}

};
class IGallery
{

public:
	IGallery();
	virtual ~IGallery();

	bool Initalize();
	void Shutdown();
	static void SaveData();
	bool LoadData();


	static bool CreateArtist(Artist* &pArtist);
	static bool CreateClient(Client* &pClient);
	static bool CreateExposition(Exposition* &pExposition, Owner* owner);
	static bool CreateReservation(Reservation* &pReservation, Owner* owner);
	static bool CreateHandicraft(Handicraft* &pHandicraft);
	static bool CreatePaiting(Painting* &pPainting);
	static bool CreatePhoto(Photo* &pPhoto);
	static bool CreateSculpture(Sculpture* &pSculpture);
	static bool CreateDate(Date& date);
	static bool CreateTransaction(Transaction& pTransaction);
	static bool CreateOwner(Owner* pOwner);
	static bool CreatePiece(Piece* pPiece);

	static bool GetArtist(const std::string& name, Artist* &pArtist);
	static bool GetClient(const std::string& name, Client* &pClient);
	static bool GetInactiveClient(const std::string& name, Client* &pClient);
	static bool GetOwner(const std::string& name, Owner* &pOwner);
	static bool GetExposition(unsigned int ID, Exposition* &pExposition);
	static bool GetReservation(unsigned int ID, Reservation* &pReservation);
	static bool GetPiece(unsigned int ID, Piece* &pPiece);
	static bool GetPiece(std::string name, Piece* &pPiece);
	static bool GetHandicraft(unsigned int ID, Handicraft* &pHandicraft);
	static bool GetPainting(unsigned int ID, Painting* &pPainting);
	static bool GetPhoto(unsigned int ID, Photo* &pPhoto);
	static bool GetSculpture(unsigned int ID, Sculpture* &pSculpture);
	static bool GetPerson(const std::string& name, Person* &pPerson);
	static void GetGallery(Gallery* &pGallery);
	static int GetPeriod();

	static void SetExpositions(std::vector<Exposition*> &pExposition);
	static void SetReservations(std::priority_queue<Reservation*> &pReservation);
	static void SetPeriod(int period);

	static bool DeleteArtist(const std::string& name);
	static bool DeleteClient(const std::string& name);
	static bool DeleteInactiveClient(const std::string& name);
	static bool DeleteHandicraft(unsigned int ID);
	static bool DeletePaintings(unsigned int ID);
	static bool DeletePhoto(unsigned int ID);
	static bool DeleteSculpture(unsigned int ID);
	static bool DeleteExposition(unsigned int ID);
	static bool DeleteReservation(unsigned int ID);
	static std::vector<Piece*> GetPieces();
	static std::vector<Exposition*> GetExpositions();
	static std::priority_queue<Reservation*> GetReservations();
	static std::vector<Owner*> GetOwners();
	template <class T>
	static void DestroyContainer(std::vector<T*>& container);

	static bool AddFilename(const std::string &filenameType, std::string &filename);

	static void UpdateTime();

	static struct tm  ts;
private:
	static bool CreateFile(const std::string& filename);

public:
	static IOGallery* mInput;
	static std::string mExpositionsFilename;
	static std::string mReservationsFilename;
	static std::string mArtistsFilename;
	static std::string mClientsFilename;
	static std::string mHandicraftsFilename;
	static std::string mPaintingsFilename;
	static std::string mPhotosFilename;
	static std::string mSculpturesFilename;
	static std::string mGalleryFilename;

private:
	static std::priority_queue<Reservation*> mReservations;
	static std::vector<Exposition*> mExpositions;
	static std::vector<Artist*> mArtists;
	static std::vector<Client*> mClients;
	static std::unordered_set<Client, HashClient, HashClient> mInactiveClients;
	static Gallery* mGallery;
	static std::vector<Handicraft*> mHandicrafts;
	static std::vector<Painting*> mPaintings;
	static std::vector<Photo*> mPhotos;
	static std::vector<Sculpture*> mSculptures;
	static int mPeriod;

};


struct FindArtist;
struct FindClient;
struct FindExposition;
struct FindHandicraft;
struct FindPainting;
struct FindPhoto;
struct FindSculpture;
struct FindTransaction;
struct FindOwner;
struct FindPerson;
struct FindPiece;


#endif // !defined(IGallery_H__INCLUDED)

