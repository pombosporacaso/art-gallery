///////////////////////////////////////////////////////////
//  Piece.h
//  Implementation of the Class Piece
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Piece_H__INCLUDED)
#define Piece_H__INCLUDED

#ifndef string
#include <string>
#endif

#ifndef vector
#include <vector>
#endif

#ifndef iostream
#include <iostream>
#endif

class Owner;
class Artist;
class Transaction;
class Date;
bool operator<(const Owner& o1, const Owner& o2);

class Piece
{

public:
	/**
	* Constructor that creates a default piece
	*
	*/
	Piece();

	/**
	* Destructor that eliminates the space occupied by an piece
	*
	*/
	virtual ~Piece();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*@return returns returns true or false whether the class was succesfully intialized
	*/
	virtual bool Initialize(const std::string& filename) = 0;

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	virtual void Shutdown() = 0;

	/**
	* Function that adds an object of the class Transaction to the private member variables mTransactions
	*@param transaction that will be added
	*/
	void AddTransaction(Transaction transaction);

	/**
	* Function returns the artist who is the author of the piece
	*
	*@return returns the name of the author of the piece
	*/
	const std::string& GetAuthor() const;

	/**
	* Function returns the owner of the piece
	*
	*@return returns the owner as an object of the Owner class
	*/
	Owner& GetOwner() const;

	/**
	* Pure virtual function that returns the id of the piece depending on it's type
	*
	*@return returns the integer corresponding to the piece's type ID
	*/
	virtual unsigned int GetTypeID() const = 0;

	/**
	* Function that returns the id of the piece
	*
	*@return returns the integer corresponding to the piece ID
	*/
	unsigned int GetID() const;

	/**
	* Function returns the size of the piece as "big","medium" or "small"
	*
	*@return returns the piece size as a string
	*/
	const std::string& GetSize() const;

	/**
	*Function that returns all of the transactions that include the current piece
	*
	*@return returns vector of objects from Transaction class where the transactions are stored
	*/
	const std::vector<Transaction>& GetTransactions() const;

	/**
	* Pure virtual function that returns the type of the piece as "Handicraft", "Painting", "Sculpture" or "Photo"
	*
	*@return returns the piece type as a string
	*/
	virtual std::string GetType() const = 0;

	/**
	*Function in charge of making the name of the file where the piece is stored by using it's ID and type
	*@param filename where the string with the name of the file will be stored
	*/
	void GetFilename(std::string& filename) const;

	/**
	* Function that returns the year of the creation of the piece
	*
	*@return returns the year as an integer
	*/
	int GetYear() const;

	/**
	* Function that returns the name of the piece
	*
	*@return returns the name as a string
	*/
	const std::string& GetName() const;

	/**
	* Function that returns true if the piece is for sale or false if otherwise
	*
	*@return returns true if the piece is for sale or false if otherwise
	*/
	bool IsForSale() const;

	/**
	* Function that returns true if the piece is authorized to appear in expositions or false if otherwise
	*
	*@return returns true if the piece is authorized to appear in expositions or false if otherwise
	*/
	bool isAuthorized() const;

	/**
	* Function that sets the name of the author of the piece
	*@param author name
	*/
	void SetAuthor(const std::string& author);

	/**
	* Function that will set the piece for sale or make it not for sale
	*@param isForSale true if the piece will be set for sale or false otherwise
	*/
	void SetForSale(bool isForSale);

	/**
	* Function that sets the owner of the piece as a pointer to a given object of the class Owner
	*@param owner object of the class Owner that will be set as the owner
	*/
	void SetOwner(Owner* owner);

	/**
	* Function that will set the piece size by receiving strings as "big","medium" or "small"
	*@param size string that will be used to set the piece's size
	*/
	bool SetSize(const std::string& size); 

	/**
	* Function that will set the piece's year of creation
	*@param year that will be set as the piece's year of creation
	*/
	void SetYear(int year);

	/**
	* Function that will set the piece's name
	*@param name string that will be used to set the piece's name
	*/
	void SetName(const std::string& name);

	/**
	* Function that returns true if the two objects of the class Piece have the same values in their variable members or false otherwise
	*@param piece that will be used for comparision
	*@return returns true if the two pieces are equal
	*/
	bool operator==(const Piece& piece) const;

	/**
	* Function that will set the piece authorized to appear in expositions or unauthorized
	*@param isForSale true if the piece will be set authorized to appear or false otherwise
	*/
	void SetExpAuthorized(bool isAuth);

	/**
	* Function that returns true if the piece is authorized to appear in expositions or false if otherwise
	*
	*@return returns true if the piece is authorized to appear in expositions or false if otherwise
	*/
	bool GetExpAuthorized();

	static bool InputSize(std::string& pSize);

	friend bool operator<(const Piece& p1, const Piece &p2)
	{
		if (p1.GetYear() != p2.GetYear())
			return p1.GetYear() < p2.GetYear();
		if (p1.GetType() != p2.GetType())
			return p1.GetType() < p2.GetType();
		if (p1.GetAuthor() != p2.GetAuthor())
			return p1.GetAuthor() < p2.GetAuthor();
		return p1.GetOwner() < p2.GetOwner();
	}

protected:
	bool LoadData(const std::string &filename);
	bool SaveData(const std::string &filename) const;

private:
	std::string mAuthor;
	bool mForSale;
	Owner* mOwner;
	std::string mSize;
	static unsigned int msNumberPieces;
	unsigned int mID;
	std::vector<Transaction> mTransactions;
	int mYear;
	std::string mName;
	bool mExpAuthorized;
};

std::ostream& operator<<(std::ostream& os, const Piece& piece);
std::ofstream& operator<< (std::ofstream& os, const Piece& piece);
std::ifstream& operator>> (std::ifstream& is, Piece& piece);

#endif // !defined(Piece_H__INCLUDED)
