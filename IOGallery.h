#if !defined(IOGallery_H__INCLUDED)
#define IOGallery_H__INCLUDED

#ifndef sstream
#include <sstream>
#endif // !sstream

#ifndef iostream
#include <iostream>
#endif // !iostream


class IOGallery
{
public:
	IOGallery();

	IOGallery& operator>>(std::string &input);
	IOGallery& operator>>(int &input);
	IOGallery& operator>>(unsigned int &input);
	IOGallery& operator>>(double &input);
	bool InputYesNo(char &input);
	bool InputTheme(std::string &input);
	bool InputPassword(std::string &input);
	bool InputOwner(std::string &input);

	operator bool();

private:
	bool IsValidKey(char key, char type);
	bool IOGallery::GetInput(char type, std::stringstream& ss);

private:
	static bool mValid;
};

#endif // !IOGallery_H__INCLUDED
