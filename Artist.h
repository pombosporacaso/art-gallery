///////////////////////////////////////////////////////////
//  Artist.h
//  Implementation of the Class Artist
//  Created on:      06-Nov-2013 1:03:11 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Artist_H__INCLUDED)
#define Artist_H__INCLUDED

#include "Person.h"

#ifndef iostream
#include <iostream>
#endif // !iostream

class Artist : public Person
{

public:

	/**
	* Constructor that creates a default artist
	*
	*/
	Artist();

	/**
	* Destructor that eliminates the space occupied by an artist
	*
	*/
	~Artist();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*@return returns true or false whether the class was succesfully intialized
	*/
	bool Initialize(const std::string& filename);

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	void Shutdown();

	/**
	* Function in charge of allowing an artist to create a new piece
	*
	*
	*@return returns true or false whether the piece was succesfully created or not
	*/
	bool CreatePiece();

	/**
	* Function that allows the artist to store a piece in the gallery
	*
	*@param ID of the piece to be stored
	*@return returns true or false whether the piece was succesfully stored or not
	*/
	bool StorePiece(unsigned int ID);

	/**
	* Function returns the ID of the artist
	*
	*@return returns the ID of the artist
	*/
	unsigned int GetID() const;

	/**
	* Function that tells us what type of owner we're dealing with
	*
	*@return the owner_t Artist_o, indicating we're dealing with an artist
	*/
	owner_t GetType() const;

	void GetFilename(std::string& filename) const;

private:
	unsigned int mArtistID;
	static unsigned int msNumberArtists;

	bool LoadData(const std::string& filename);
	bool SaveData(const std::string& filename) const;
};
#endif // !defined(Artist_H__INCLUDED)
