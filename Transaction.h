///////////////////////////////////////////////////////////
//  Transaction.h
//  Implementation of the Class Transaction
//  Created on:      06-Nov-2013 1:03:15 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Transaction_H__INCLUDED)
#define Transaction_H__INCLUDED

#include "Date.h"

class Owner;

class Transaction
{

public:
	/**
	* Constructor that creates a default Transaction
	*
	*/
	Transaction();

	/**
	* Destructor that eliminates the space occupied by an Transaction
	*
	*/
	~Transaction();

	/**
	* Function returns the buyer of the piece
	*
	*@return returns the buyer as an object of the Owner class
	*/
	const Owner& GetBuyer() const;

	/**
	* Function returns the date of the transaction
	*
	*@return returns the date as an object of the class Date
	*/
	const Date& GetDate() const;

	/**
	* Function returns the ID of the transaction
	*
	*@return returns the ID as an integer
	*/
	unsigned int GetID() const;

	/**
	* Function returns the price paid in the transaction
	*
	*@return returns the price as a double
	*/
	double GetPrice() const;

	/**
	* Function returns the seller of the piece
	*
	*@return returns the seller as an object of the Owner class
	*/
	const Owner& GetSeller() const;

	/**
	* Function that sets the buyer of the piece as a pointer to a given object of the class Owner
	*@param buyer object of the class Owner that will be set as the buyer
	*/
	void SetBuyer(Owner &buyer);

	/**
	* Function that will set the transaction's date
	*@param date that will be set as the transaction's date
	*/
	void SetDate(const Date& date);

	/**
	* Function that will set the transaction's price
	*@param price that will be set as the transaction's price
	*/
	void SetPrice(double price);

	/**
	* Function that sets the seller of the piece as a pointer to a given object of the class Owner
	*@param seller object of the class Owner that will be set as the seller
	*/
	void SetSeller(Owner& seller);
	bool operator==(const Transaction& transaction) const;

private:
	Owner* mBuyer;
	Date mDate;
	unsigned int mID;
	double mPrice;
	Owner* mSeller;
	static unsigned int msNumberTransactions;
};

std::ostream& operator<<(std::ostream& os, const Transaction& transaction);
std::ofstream& operator<< (std::ofstream& os, const Transaction& transaction);
std::ifstream& operator>> (std::ifstream& is, Transaction& transaction);

#endif // !defined(Transaction_H__INCLUDED)
