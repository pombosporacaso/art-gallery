///////////////////////////////////////////////////////////
//  Photo.cpp
//  Implementation of the Class Photo
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Photo.h"
#include "Exceptions.h"
#include "IGallery.h"

using namespace std;

unsigned int Photo::msNumberPhotos = 0;

Photo::Photo()
{
	msNumberPhotos++;
	mPhotoID = msNumberPhotos;
}
Photo::~Photo()
{
	msNumberPhotos--;
}

bool Photo::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Photo::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);
	IGallery::AddFilename(IGallery::mPhotosFilename, filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

string Photo::GetType() const
{
	return "Photo";
}
unsigned int Photo::GetTypeID() const
{
	return mPhotoID;
}