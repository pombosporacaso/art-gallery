///////////////////////////////////////////////////////////
//  Person.h
//  Implementation of the Class Person
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Person_H__INCLUDED)
#define Person_H__INCLUDED

#include "Owner.h"

class Exposition;

class Person : public Owner
{

public:

/**
 * Constructor that creates a default person (a person can be either a client or an artist)
 *
 */
	Person();

/**
 * Destructor that eliminates the space occupied by a person
 *
 */
	virtual ~Person();

/**
 * Function capable of asking which piece does the person want to authorize to be in an exposition
 *
 *@param takes as argument a single piece to be authorized
 *@return returns true or false whether the piece was succesfully authorized
 */
	bool AuthExposition(Piece* piece);

/**
 * Function that allows a person to create an exposition
 *
 *@return returns true or false whether the exposition was succesfully created
 */
	bool CreateExposition();


private:
	
};
#endif // !defined(Person_H__INCLUDED)
