///////////////////////////////////////////////////////////
//  Artist.cpp
//  Implementation of the Class Artist
//  Created on:      06-Nov-2013 1:03:11 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Artist.h"
#include "Exceptions.h"
#include "Gallery.h"
#include "IGallery.h"
#include "IOGallery.h"
#include "Painting.h"
#include "Photo.h"
#include "Handicraft.h"
#include "Sculpture.h"

#ifndef sstream
#include <sstream>
#endif // !sstream

#ifndef fstream
#include <fstream>
#endif // !fstream

#ifndef algorithm
#include <algorithm>
#endif

using namespace std;

unsigned int Artist::msNumberArtists = 0;

Artist::Artist()
{
	msNumberArtists++;
	mArtistID = msNumberArtists;
}
Artist::~Artist()
{
	msNumberArtists--;
}

bool Artist::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Artist::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);
	IGallery::AddFilename(IGallery::mArtistsFilename, filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

bool Artist::LoadData(const string& filename)
{
	ifstream inFile;
	inFile.open(filename, ios::in);
	if (!inFile)
		return false;

	inFile >> *this;

	inFile.close();

	return true;
}
bool Artist::SaveData(const string& filename) const
{
	ofstream outFile;
	outFile.open(filename, ios::out);
	if (!outFile)
		return false;

	outFile << *this;

	outFile.close();

	return true;
}
void Artist::GetFilename(std::string& filename) const
{
	stringstream ss;

	ss << "Artist_" << mArtistID << ".txt";

	ss >> filename;
}

bool Artist::CreatePiece()
{
	string type;
	cout << "\t Type: (Paintings / Photos / Sculptures / Handicrafts)\n\t";
	if (!IGallery::mInput->InputTheme(type))
		return false;

	if (type == "Paintings")
	{
		Painting* pPainting = 0;
		if (!IGallery::CreatePaiting(pPainting))
			return false;
		pPainting->SetAuthor(GetName());
	}
	else if (type == "Photos")
	{
		Photo* pPhoto = 0;
		if (!IGallery::CreatePhoto(pPhoto))
			return false;
		pPhoto->SetAuthor(GetName());
	}
	else if (type == "Sculptures")
	{
		Sculpture* pSculpture = 0;
		if (!IGallery::CreateSculpture(pSculpture))
			return false;
		pSculpture->SetAuthor(GetName());
	}
	else if (type == "Handicrafts")
	{
		Handicraft* pHandicraft = 0;
		if (!IGallery::CreateHandicraft(pHandicraft))
			return false;
		pHandicraft->SetAuthor(GetName());
	}
		
	return true;
}
SPiece find(BST<SPiece> bst, int id);
bool Artist::StorePiece(unsigned int ID)
{
	Gallery* pGallery = 0;
	IGallery::GetGallery(pGallery);


	BST<SPiece> sto = pGallery->GetPiecesStorage();
	SPiece sp;
	sp = find(sto, ID);


	Piece* asd = 0;
	SPiece NotF(asd);
	if (sp == NotF)
		return false;
	pGallery->AddPieceStorage(ID);

	return true;
}

unsigned int Artist::GetID() const
{
	return mArtistID;
}
owner_t Artist::GetType() const
{
	return Artist_o;
}