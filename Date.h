///////////////////////////////////////////////////////////
//  Date.h
//  Implementation of the Class Date
//  Created on:      06-Nov-2013 1:03:12 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Date_H__INCLUDED)
#define Date_H__INCLUDED

#ifndef iostream
#include <iostream>
#endif // !iostream

#ifndef fstream
#include <fstream>
#endif // !fstream


class Date
{

public:
	Date();
	~Date();

	unsigned int GetDay() const;
	unsigned int GetHours() const;
	unsigned int GetMinutes() const;
	unsigned int GetMonth() const;
	int GetYear() const;
	bool SetDay(unsigned int day);
	bool SetHours(unsigned int hours);
	bool SetMinutes(unsigned int minutes);
	bool SetMonth(unsigned int month);
	void SetYear(int year);

	static bool InputDate(Date& date);

	bool operator==(const Date& date) const;

private:
	unsigned int mDay;
	unsigned int mHours;
	unsigned int mMinutes;
	unsigned int mMonth;
	int mYear;
};

std::ostream& operator<< (std::ostream& os, const Date& date);
std::ofstream& operator<< (std::ofstream& os, const Date& date);
std::ifstream& operator>> (std::ifstream& is, Date& date);

#endif // !defined(Date_H__INCLUDED)
