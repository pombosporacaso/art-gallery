///////////////////////////////////////////////////////////
//  Exposition.cpp
//  Implementation of the Class Exposition
//  Created on:      06-Nov-2013 1:03:12 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Exposition.h"
#include "Piece.h"
#include "IGallery.h"
#include "Exceptions.h"
#include "Owner.h"

#ifndef sstream
#include <sstream>
#endif // !sstream

#ifndef fstream
#include <fstream>
#endif // !fstream

#ifndef algorithm
#include <algorithm>
#endif // !algorithm


using namespace std;

unsigned int Exposition::msNumberExpositions = 0;

Exposition::Exposition()
{
	msNumberExpositions++;
	mID = msNumberExpositions;
}
Exposition::~Exposition()
{
	msNumberExpositions--;
}

bool Exposition::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Exposition::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

bool Exposition::LoadData(const std::string& filename)
{
	ifstream inFile;
	inFile.open(filename, ios::in);
	if (!inFile)
		return false;

	inFile >> *this;

	inFile.close();

	return true;
}
bool Exposition::SaveData(const std::string& filename) const
{
	ofstream outFile;
	outFile.open(filename, ios::out);
	if (!outFile)
		return false;

	outFile << *this;

	outFile.close();

	return true;
}
void Exposition::GetFilename(std::string& filename) const
{
	stringstream ss;

	ss << "Exposition_" << mID << ".txt";

	ss >> filename;
}

bool Exposition::AddPiece(Piece* piece)
{
	vector<Piece*>::iterator it;
	it = find_if(mPieces.begin(), mPieces.end(), [&](Piece* p)
	{
		return p->GetID() == piece->GetID();
	});
	if (it != mPieces.end())
		return false;

	mPieces.push_back(piece);
	return true;
}

const Date& Exposition::GetDateBegin() const
{
	return mDateBegin;
}
const Date& Exposition::GetDateEnd() const
{
	return mDateEnd;
}
unsigned int Exposition::GetID() const
{
	return mID;
}
const vector<Piece*>& Exposition::GetPieces() const
{
	return mPieces;
}
unsigned int Exposition::GetSpace() const
{
	return mSpace;
}
const string& Exposition::GetType() const
{
	return mType;
}
string Exposition::GetOwner() const
{
	return mOwner;
}

void Exposition::SetDateBegin(const Date& dateBegin)
{
	mDateBegin = dateBegin;
}
void Exposition::SetDateEnd(const Date& dateEnd)
{
	mDateEnd = dateEnd;
}
void Exposition::SetPieces(const vector<Piece*>& pieces)
{
	mPieces = pieces;
}
void Exposition::SetSpace(unsigned int space)
{
	mSpace = space;
}
void Exposition::SetThematic(bool enable)
{
	mThematic = enable;
}
void Exposition::SetType(const string& type)
{
	mType = type;
}
void Exposition::SetOwner(const string& name)
{
	mOwner = name;
}

bool Exposition::IsThematic() const
{
	return mThematic;
}

bool Exposition::operator==(const Exposition& exposition) const
{
	return mID == exposition.GetID();
}
bool Exposition::operator<(const Exposition& exp) const
{
	Owner* owner1 = 0;
	if (!IGallery::GetOwner(mOwner, owner1))
		return false;

	Owner* owner2 = 0;
	if (!IGallery::GetOwner(exp.mOwner, owner2))
		return false;

	return owner1 < owner2;
}
bool Exposition::AddPieces(vector<Piece*> gPieces)
{
	int pID;

	system("cls");
	cout << "\tWhich pieces do you want to exhibit?\n\t\tID: (0 to finish input)\n";


	do{
		cin >> pID;
		vector<Piece*>::iterator it;
		if (pID != 0)
		{
			for (it = gPieces.begin(); it != gPieces.end(); it++)
			{
				if ((*it)->GetID() == pID)
				if (gPieces.size() == IGallery::GetPieces().size())
				{
					if ((*it)->GetExpAuthorized())

						break;
				}
				else
					break;
			}
			if (it != gPieces.end())
				AddPiece(*it);
			else
				cout << "\n\n\t\tYou cannot add this piece\n";
		}
	} while (pID != 0);

	if (mPieces.empty())
		return false;
	else
		return true;
}
bool Exposition::AddPieces(const std::string& theme, vector<Piece*> gPieces)
{
	int pID;

	system("cls");
	cout << "\tWhich pieces do you want to exhibit?\n\t\tID: (0 to finish input)\n";

	do
	{
		cin >> pID;
		vector<Piece*>::iterator it;

		if (pID != 0)
		{
			for (it = gPieces.begin(); it != gPieces.end(); it++)
			{
				if ((*it)->GetID() == pID && (*it)->GetType() == theme)
				if (gPieces.size() == IGallery::GetPieces().size())
				{
					if ((*it)->GetExpAuthorized())
						break;
				}
				else
					break;
			}
			if (it != gPieces.end())
				AddPiece(*it);
			else
				cout << "\n\n\t\tYou cannot add this piece\n";
		}
	} while (pID != 0);

	if (mPieces.empty())
		return false;
	else
		return true;
}

ostream& operator<<(ostream& os, const Exposition& exposition)
{
	os << "\n\t Exposition \n";
	os << "\n\t Begin Date" << exposition.GetDateBegin();
	os << "\n\t End Date" << exposition.GetDateEnd();
	os << "\n\t Space:\t\t " << exposition.GetSpace();
	os << "\n\t Thematic:\t " << exposition.IsThematic();
	os << "\n\t Type:\t\t" << exposition.GetType();

	return os;
}
ofstream& operator<< (ofstream& os, const Exposition& exposition)
{
	os << exposition.GetDateBegin();
	os << exposition.GetDateEnd();
	os << exposition.GetSpace() << endl;
	os << exposition.GetType() << endl;
	os << exposition.GetOwner() << endl;

	vector<Piece*>::iterator it;
	vector<Piece*> pieces = exposition.GetPieces();
	for (it = pieces.begin(); it != pieces.end(); it++)
		os << (*it)->GetID() << ',';

	return os;
}
ifstream& operator>> (ifstream& is, Exposition& exposition)
{
	Date dateBegin;
	is >> dateBegin;
	exposition.SetDateBegin(dateBegin);

	Date dateEnd;
	is >> dateEnd;
	exposition.SetDateEnd(dateEnd);

	unsigned int space;
	is >> space;
	exposition.SetSpace(space);

	string type;
	is >> type;
	exposition.SetType(type);

	is >> type;
	exposition.SetOwner(type);

	string data;
	unsigned int ID;
	while (getline(is, data, ','))
	{
		ID = stoi(data);

		Piece* pPiece = 0;
		if (IGallery::GetPiece(ID, pPiece))
			exposition.AddPiece(pPiece);
	}

	return is;
}