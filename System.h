/////////////////////////////////////////////////////////
//  System.h
//  Implementation of the Class System
//  Created on:      06-Nov-2013 1:03:15 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(System_H__INCLUDED)
#define System_H__INCLUDED

#include "IGallery.h"

enum menus_t 
{ 
	Preview_m, 

	LoginMenu_m, 
	MainMenu_m,
	SearchMenu_m,
	ArtistMenu_m,
	ArtistMenu2_m,
	ClientMenu_m,
	AdminMenu_m,

	Exit_m,
};

class System
{

public:
	System();
	~System();

	bool Initialize();
	void Run();
	void Shutdown();
private:
	IGallery* mIGallery;

	void Preview();
	void LoginMenu();
	bool LoginUser();
	bool RegisterUser();
	void MainMenu();
	void AdminMenu();
	void AdminMenuNP();
	void ClientMenu();
	void ArtistMenu();
	void ArtistMenuNP();
	void SearchMenu();
	void SearchPieces();
	bool SellPiece();
	bool CreateReservation();
	bool RemoveArtist();
	bool RemoveClient();
	bool RemovePieceForSale();
	bool RemovePieceStorage();
	bool ShowInactiveClients();
	bool BuyPiece();
	void UpTime();
	bool CreatePiece();
	bool StorePiece();
	bool AuthorizePiece();
	bool UnauthorizePiece();
	bool RemovePiece();
	bool SearchPiecesID();
	bool SearchPiecesName();
	bool SearchPiecesType();
	bool SearchPiecesOwner();
	bool SearchPiecesAuthor(); 
	void ShowPieces();
	void ShowExpositions();
	void ShowPiecesForSale();
	void ShowPiecesStorage();
	void ShowOwnedPieces();
	bool ShowTransactions();

	menus_t mCurrentMenu;
	Owner* mpCurrentUser;

};
#endif // !defined(System_H__INCLUDED)
