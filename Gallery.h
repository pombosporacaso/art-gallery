///////////////////////////////////////////////////////////
//  Gallery.h
//  Implementation of the Class Gallery
//  Created on:      06-Nov-2013 1:03:12 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Gallery_H__INCLUDED_)
#define Gallery_H__INCLUDED_


#include "Owner.h"
#include "Reservation.h"

#ifndef queue
#include <queue>
#endif //!queue

#include "BST.h"

class Date;
class Exposition;
class Piece;

bool operator<(const Piece& p1, const Piece &p2);

struct SPiece
{
	Piece* piece;
	SPiece()
	{
		this->piece = 0;
	}
	SPiece(Piece* p)
	{
		this->piece = p;
	}
	bool operator <(const SPiece &p) const
	{
		return *piece < *p.piece;
	}
	bool operator ==(const SPiece &p) const
	{
		return piece == p.piece;
	}
};
class Gallery : public Owner
{

public:

	/**
	* Constructor that creates a default gallery
	*
	*/
	Gallery();

	/**
	* Destructor that eliminates the space occupied by a gallery
	*
	*/
	~Gallery();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*/
	bool Initialize(const std::string& filename);

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	void Shutdown();

	/**
	* Function capable of adding an exposition to the vector of expositions
	*
	*@param receives the space that the expo will occupy
	*@param receives a bool indicating whether the expo is themed or not
	*@param receives the vector of pieces to add to the expo
	*@param receives the begining and end date of the expo
	*@return returns true or false whether the exposition was succesfully added or not
	*/
	bool AddExposition(int space, bool thematic, const std::vector<Piece*>& pieces, const Date& dateEnd, const Date& dateBegin, std::string name);

	bool AddReservation(const Date& date,string owner, Exposition* expo);

	/**
	* Function capable of putting a piece for sale
	*
	*@param receives the ID of the piece to be sold
	*@return returns true or false whether the piece was succesfully added or not
	*/
	bool AddPieceForSale(unsigned int ID);

	/**
	* Function capable of storing a piece
	*
	*@param receives the ID of the piece to be stored
	*@return returns true or false whether the piece was succesfully stored or not
	*/
	bool AddPieceStorage(unsigned int ID);

	/**
	* Function that allows the Gallery to create an exposition
	*
	*@return returns true or false whether the exposition was succesfully created
	*/
	bool CreateExposition();

	/**
	* Function that tells us what type of owner we're dealing with
	*
	*@return the owner_t Gallery_o, indicating we're dealing with the Gallery
	*/
	owner_t GetType() const;

	/**
	*Function that returns the vector of expositions
	*
	*@return returns a vector with the expositions
	*/
	const std::vector< Exposition*>& GetExpositions() const;

	std::priority_queue< Reservation*> GetReservations() const;

	/**
	*Function that returns the vector of pieces for sale
	*
	*@return returns a vector with the pieces for sale
	*/
	const std::vector<Piece*>& GetPiecesForSale() const;

	/**
	*Function that returns the vector of pieces in storage
	*
	*@return returns a vector with the pieces in storage
	*/
	const BST<SPiece>& GetPiecesStorage() const;

	/**
	* Function capable of removing an exposition from the vector of owned expositions
	*
	*@param receives the ID of the exposition to be removed
	*@return returns true or false whether the exposition was succesfully removed or not
	*/
	bool RemoveExposition(unsigned int ID);

	bool RemoveReservation(unsigned int ID);

	/**
	* Function capable of removing a piece from the vector of pieces for sale
	*
	*@param receives the ID of the piece to be removed
	*@return returns true or false whether the piece was succesfully removed or not
	*/
	bool RemovePieceForSale(unsigned int ID);

	/**
	* Function capable of removing a piece from the vector of pieces in storage
	*
	*@param receives the ID of the piece to be removed
	*@return returns true or false whether the piece was succesfully removed or not
	*/
	bool RemovePieceStorage(unsigned int ID);

	/**
	* Function in charge of changing a exposition
	*
	*@param receives the new exposition
	*@param receives the index of the expo to be changed
	*@return returns true or false whether the exposition was succesfully changed or not
	*/
	bool SetExposition(Exposition* exp, unsigned int index);

	bool SetReservation(Reservation* res, unsigned int ID);

	/**
	* Function in charge of changing a piece for sale
	*
	*@param receives the new piece
	*@param receives the index of the piece to be changed
	*@return returns true or false whether the piece was succesfully changed or not
	*/
	bool SetPieceForSale(Piece* piece, unsigned int index);

	/**
	* Function in charge of changing a piece for storage
	*
	*@param receives the new piece
	*@param receives the index of the piece to be changed
	*@return returns true or false whether the piece was succesfully changed or not
	*/
	bool SetPieceStorage(Piece* piece, unsigned int index);

	/**
	* Function in charge of calculating the available space in the gallery in the given date
	*
	*@param receives the date with which to calculate the space
	*@return returns the amount of space, in %, available
	*/
	unsigned int AvailableSpace(Date bDate);

	void GetFilename(std::string& filename) const;

private:
	std::vector<Exposition*> mExpositions;
	std::vector<Piece*> mPiecesForSale;
	BST<SPiece> mStorage;
	std::priority_queue<Reservation*> mReservations;
	bool LoadData(const std::string& filename);
	bool SaveData(const std::string& filename) const;
};

std::ostream& operator<<(std::ostream& os, const Gallery& gallery);
std::ofstream& operator<< (std::ofstream& os, const Gallery& gallery);
std::ifstream& operator>> (std::ifstream& is, Gallery& gallery);

#endif // !defined(Gallery_H__INCLUDED_)
