///////////////////////////////////////////////////////////
//  Photo.h
//  Implementation of the Class Photo
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Photo_H__INCLUDED)
#define Photo_H__INCLUDED

#include "Piece.h"

class Photo : public Piece
{

public:
	/**
	* Constructor that creates a default Photo
	*
	*/
	Photo();

	/**
	* Destructor that eliminates the space occupied by an Photo
	*
	*/
	~Photo();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*@return returns returns true or false whether the class was succesfully intialized
	*/
	bool Initialize(const std::string& filename);

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	void Shutdown();

	/**
	* Function that tells us what type of piece we're dealing with
	*
	*@return returns a string "Photo", indicating we're dealing with an Photo
	*/
	std::string GetType() const;

	/**
	* Pure virtual function that returns the id of the piece depending on it's type
	*
	*@return returns the integer corresponding to the piece's type ID
	*/
	unsigned int GetTypeID() const;

private:
	unsigned int mPhotoID;
	static unsigned int msNumberPhotos;
};
#endif // !defined(Photo_H__INCLUDED)
