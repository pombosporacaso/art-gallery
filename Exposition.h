///////////////////////////////////////////////////////////
//  Exposition.h
//  Implementation of the Class Exposition
//  Created on:      06-Nov-2013 1:03:12 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Exposition_H__INCLUDED)
#define Exposition_H__INCLUDED

#include "Date.h"

#ifndef vector
#include <vector>
#endif

#ifndef string
#include <string>
#endif 


class Piece;

class Exposition
{

public:
	Exposition();
	virtual ~Exposition();

	bool Initialize(const std::string &filename);
	void Shutdown();

	bool AddPiece(Piece* piece);
	bool AddPieces(const std::string& theme, std::vector<Piece*> gPieces); // Pergunta ao utilizador o nome da pe�a ou ID que querem adicionar, num loop infinito at� parar
	bool AddPieces(std::vector<Piece*> gPieces); //Caso nao tenha theme NOTA: gPieces refere se ao vetor com pe�as a analisar
	const Date& GetDateBegin() const;
	const Date& GetDateEnd() const;
	unsigned int GetID() const;
	const std::vector<Piece*>& GetPieces() const;
	unsigned int GetSpace() const;
	std::string GetOwner() const;
	const std::string& GetType() const;
	void SetDateBegin(const Date& dateBegin);
	void SetDateEnd(const Date& dateEnd);
	void SetPieces(const std::vector<Piece*>& pieces);
	void SetSpace(unsigned int space);
	void SetThematic(bool enable); 
	void SetType(const std::string& type); 
	void SetOwner(const std::string& name);
	bool operator==(const Exposition& exposition) const;
	bool operator<(const Exposition& exp) const;
	bool IsThematic() const;
	void GetFilename(std::string& filename) const;

private:
	Date mDateBegin;
	Date mDateEnd;
	std::string mOwner;
	unsigned int mID;
	std::vector<Piece*> mPieces;
	static unsigned int msNumberExpositions;
	unsigned int mSpace;
	bool mThematic;
	std::string mType;

	bool LoadData(const std::string& filename);
	bool SaveData(const std::string& filename) const;
};

std::ostream& operator<<(std::ostream& os, const Exposition& exposition);
std::ofstream& operator<< (std::ofstream& os, const Exposition& exposition);
std::ifstream& operator>> (std::ifstream& is, Exposition& exposition);

#endif // !defined(Exposition_H__INCLUDED)
