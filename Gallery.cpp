///////////////////////////////////////////////////////////
//  Gallery.cpp
//  Implementation of the Class Gallery
//  Created on:      06-Nov-2013 1:03:12 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Gallery.h"
#include "Exposition.h"
#include "Piece.h"
#include "IGallery.h"
#include "Exceptions.h"
#include "Owner.h"

#ifndef sstream
#include <sstream>
#endif // !sstream

#ifndef fstream
#include <fstream>
#endif // !fstream

#ifndef algorithm
#include <algorithm>
#endif // !algorithm




using namespace std;

Piece* asd = 0;
SPiece NotF(asd);

Gallery::Gallery() :mStorage(NotF)
{
	SetName("admin");
	SetPassword("admin");
}
Gallery::~Gallery()
{
}

bool Gallery::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Gallery::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

bool Gallery::LoadData(const std::string& filename)
{
	ifstream inFile;
	inFile.open(filename, ios::in);
	if (!inFile)
		return false;

	inFile >> *this;

	inFile.close();

	return true;
}
bool Gallery::SaveData(const std::string& filename) const
{
	ofstream outFile;
	outFile.open(filename, ios::out);
	if (!outFile)
		return false;

	outFile << *this;

	outFile.close();

	return true;
}

owner_t Gallery::GetType() const
{
	return Gallery_o;
}

void Gallery::GetFilename(std::string& filename) const
{
	filename = IGallery::mGalleryFilename;
}

bool Gallery::AddExposition(int space, bool thematic, const vector<Piece*>& pieces, const Date& dateEnd, const Date& dateBegin, string name)
{
	Exposition exp;
	exp.SetDateBegin(dateBegin);
	exp.SetDateEnd(dateEnd);
	exp.SetPieces(pieces);
	exp.SetSpace(space);
	exp.SetThematic(thematic);
	exp.SetOwner(name);

	vector<Exposition*>::iterator it;
	for (it = mExpositions.begin(); it != mExpositions.end(); it++)
	{
		if (*(*it) == exp)
			break;
	}

	if (it == mExpositions.end())
	{
		mExpositions.push_back(&exp);
		return true;
	}
	else
		return false;
}
bool Gallery::AddReservation(const Date& date, string owner, Exposition* expo)
{
	Reservation res;
	res.SetDate(date);
	res.SetOwner(owner);
	res.SetExposition(expo);
	queue<Reservation*> temp;

	while (!mReservations.empty())
	{
		if (*(mReservations.top()) == res)
		{
			while (!temp.empty())
			{
				mReservations.push(temp.front());
				temp.pop();
			}
			break;
		}
		temp.push(mReservations.top());
		mReservations.pop();
	}


	if (mReservations.empty())
	{
		while (!temp.empty())
		{
			mReservations.push(temp.front());
			temp.pop();
		}
		mReservations.push(&res);
		return true;
	}
	else
		return false;
}
bool Gallery::AddPieceForSale(unsigned int ID)
{
	vector<Piece*>::iterator it;

	it = find_if(mPiecesForSale.begin(), mPiecesForSale.end(), [&](Piece* piece)
	{
		return piece->GetID() == ID;
	});

	if (it != mPiecesForSale.end())
		return false;

	Piece* pPiece = 0;
	if (!IGallery::GetPiece(ID, pPiece))
		return false;

	mPiecesForSale.push_back(pPiece);
	pPiece->SetForSale(true);

	return true;
}
SPiece find(BST<SPiece> bst, int id)
{
	BSTItrIn<SPiece> itr(bst);
	while (!itr.isAtEnd())
	{
		SPiece p;
		p = itr.retrieve();
		if ((*p.piece).GetID() == id)
			return p;
		itr.advance();
	}
	return NotF;
}
bool Gallery::AddPieceStorage(unsigned int ID)
{
	SPiece sp = find(mStorage, ID);
	if (sp == NotF)
	{
		Piece* pPiece = 0;
		if (!IGallery::GetPiece(ID, pPiece))
			return false;

		sp.piece = pPiece;
		mStorage.insert(sp);

		return true;
	}
	return false;

}

bool Gallery::CreateExposition()
{
	Exposition* exp = 0;

	return IGallery::CreateExposition(exp, this);
}
unsigned int Gallery::AvailableSpace(Date bDate)
{

	vector<Exposition*> exp = mExpositions;
	int space = 100;


	for (unsigned int i = 0; i < exp.size(); i++)
	{
		if (bDate.GetYear() < exp[i]->GetDateEnd().GetYear())
			space -= exp[i]->GetSpace();
		else
		if (bDate.GetMonth() < exp[i]->GetDateEnd().GetMonth())
			space -= exp[i]->GetSpace();
		else
		if (bDate.GetDay() < exp[i]->GetDateEnd().GetDay())
			space -= exp[i]->GetSpace();
		else
		if (bDate.GetHours() < exp[i]->GetDateEnd().GetHours())
			space -= exp[i]->GetSpace();
		else
		if (bDate.GetMinutes() <= exp[i]->GetDateEnd().GetMinutes())
			space -= exp[i]->GetSpace();
	}

	return space;
}

const vector<Exposition*>& Gallery::GetExpositions() const
{
	return mExpositions;
}
priority_queue<Reservation*> Gallery::GetReservations() const
{
	return mReservations;
}
const vector<Piece*>& Gallery::GetPiecesForSale() const
{
	return mPiecesForSale;
}
const BST<SPiece>& Gallery::GetPiecesStorage() const
{
	return mStorage;
}

bool Gallery::RemoveExposition(unsigned int ID)
{
	vector<Exposition*>::iterator it;
	for (it = mExpositions.begin(); it != mExpositions.end(); it++)
	{
		if ((*it)->GetID() == ID)
		{
			mExpositions.erase(it);
			return true;
		}
	}

	return false;
}
bool Gallery::RemoveReservation(unsigned int ID)
{
	queue<Reservation*> temp;

	while (!mReservations.empty())
	{
		if (mReservations.top()->GetID() == ID)
		{
			mReservations.pop();
			while (!temp.empty())
			{
				mReservations.push(temp.front());
				temp.pop();
			}
			return true;
		}
		temp.push(mReservations.top());
		mReservations.pop();
	}

	while (!temp.empty())
	{
		mReservations.push(temp.front());
		temp.pop();
	}

	return false;
}
bool Gallery::RemovePieceForSale(unsigned int ID)
{
	vector<Piece*>::iterator it;
	for (it = mPiecesForSale.begin(); it != mPiecesForSale.end(); it++)
	{
		if ((*it)->GetID() == ID)
		{
			mPiecesForSale.erase(it);
			return true;
		}
	}
	return false;
}
bool Gallery::RemovePieceStorage(unsigned int ID)
{
	SPiece sp = find(mStorage, ID);
	if (sp == NotF)
		return false;
	mStorage.remove(sp);
	return true;
}

bool Gallery::SetExposition(Exposition* exp, unsigned int index)
{
	if (mExpositions[index])
	{
		mExpositions[index] = exp;
		return true;
	}
	else
		return false;
}
bool Gallery::SetReservation(Reservation* res, unsigned int ID)
{
	if (RemoveReservation(ID))
	{
		mReservations.push(res);
		return true;
	}
	else
		return false;
}
bool Gallery::SetPieceForSale(Piece* piece, unsigned int index){

	if (mPiecesForSale[index])
	{
		mPiecesForSale[index] = piece;
		return true;
	}
	else
		return false;
}
bool Gallery::SetPieceStorage(Piece* piece, unsigned int index){

	return false;
}

ostream& operator<<(ostream& os, const Gallery& gallery)
{
	return os;
}
ofstream& operator<< (ofstream& os, const Gallery& gallery)
{
	os << (const Owner&)gallery;

	vector<Piece*>::const_iterator it;

	const BST<SPiece>& storage = gallery.GetPiecesStorage();
	BSTItrIn<SPiece> itr(storage);
	while (!itr.isAtEnd())
	{
		SPiece p;
		p = itr.retrieve();
		os << p.piece->GetID() << ',';
		itr.advance();
	}

	const vector<Piece*>& piecesForSale = gallery.GetPiecesForSale();
	for (it = piecesForSale.begin(); it != piecesForSale.end(); it++)
		os << (*it)->GetID() << ',';
	os << endl;

	return os;
}
ifstream& operator>> (ifstream& is, Gallery& gallery)
{
	is >> (Owner&)gallery;


	string data;
	getline(is, data, '\n');
	stringstream ss(data);

	unsigned int ID;
	while (getline(ss, data, ','))
	{
		try
		{
			ID = stoi(data);
			gallery.AddPieceStorage(ID);
		}
		catch (const exception& e)
		{
		}

	}

	getline(is, data, '\n');
	ss = stringstream(data);

	while (getline(ss, data, ','))
	{
		ID = stoi(data);

		gallery.AddPieceForSale(ID);
	}

	return is;
}
