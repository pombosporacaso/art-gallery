///////////////////////////////////////////////////////////
//  Piece.cpp
//  Implementation of the Class Piece
//  Created on:      06-Nov-2013 1:03:15 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Piece.h"
#include "Owner.h"
#include "Artist.h"
#include "Transaction.h"
#include "IGallery.h"
#include "IOGallery.h"

using namespace std;

#ifndef sstream
#include <sstream>
#endif // !sstream

unsigned int Piece::msNumberPieces = 0;

Piece::Piece()
{
	msNumberPieces++;
	mID = msNumberPieces;
}
Piece::~Piece()
{
}

bool Piece::LoadData(const std::string& filename)
{
	ifstream inFile;
	inFile.open(filename, ios::in);
	if (!inFile)
		return false;

	inFile >> *this;

	inFile.close();

	return true;
}
bool Piece::SaveData(const std::string& filename) const
{
	ofstream outFile;
	outFile.open(filename, ios::out);
	if (!outFile)
		return false;

	outFile << *this;

	outFile.close();

	return true;
}

void Piece::AddTransaction(Transaction transaction)
{
	mTransactions.push_back(transaction);
}

const string& Piece::GetAuthor() const
{
	return mAuthor;
}
Owner& Piece::GetOwner() const
{
	return *mOwner;
}
const string& Piece::GetSize() const
{
	return mSize;
}
const vector<Transaction>& Piece::GetTransactions() const
{
	return mTransactions;
}
int Piece::GetYear() const
{
	return mYear;
}
const string& Piece::GetName() const
{
	return mName;
}
void Piece::GetFilename(string &filename) const
{
	stringstream ss;

	ss << GetType() << "_" << GetTypeID() << ".txt";

	filename = ss.str();
}
bool Piece::GetExpAuthorized()
{
	return mExpAuthorized;
}
unsigned int Piece::GetID() const
{
	return mID;
}

bool Piece::IsForSale() const
{
	return mForSale;
}
bool Piece::isAuthorized() const
{
	return mExpAuthorized;
}

void Piece::SetExpAuthorized(bool isAuth)
{
	mExpAuthorized = isAuth;
}
void Piece::SetAuthor(const string& author)
{
	mAuthor = author;
}
void Piece::SetForSale(bool isForSale)
{
	mForSale = isForSale;
}
void Piece::SetOwner(Owner* owner)
{
	mOwner = owner;
}
bool Piece::SetSize(const string& size)
{
	if (size == "small" || size == "medium" || size == "large")
	{
		mSize = size;
		return true;
	}

	return false;
}
void Piece::SetYear(int year)
{
	mYear = year;
}
void Piece::SetName(const string& name)
{
	mName = name;
}

bool Piece::operator==(const Piece& piece) const
{
	return piece.GetID() == mID;
}

bool Piece::InputSize(std::string& pSize)
{
	if (!(*IGallery::mInput >> pSize))
		return false;

	return true;
}


ostream& operator<<(ostream& os, const Piece& piece)
{
	os << "\n\t " << piece.GetType() << endl;

	os << "\n\t ID:\t\t" << piece.GetID();
	os << "\n\t Name:\t\t" << piece.GetName();
	os << "\n\t Author:\t" << piece.GetAuthor();
	os << "\n\t Owner:\t\t" << piece.GetOwner().GetName();
	os << "\n\t Size:\t\t" << piece.GetSize();
	os << "\n\t Year:\t\t" << piece.GetYear();

	os << "\n\t Authorized:\t";
	if (piece.isAuthorized())
		os << "true";
	else
		os << "false";
	os << "\n\t For sale:\t";
	if (piece.IsForSale())
		os << "true";
	else
		os << "false";

	return os;
}
ofstream& operator<< (ofstream& os, const Piece& piece)
{
	os << piece.GetName() << endl;
	os << piece.GetAuthor() << endl;
	os << piece.GetOwner().GetName() << endl;
	os << piece.GetSize() << endl;
	os << piece.GetYear() << endl;
	os << piece.IsForSale() << endl;
	os << piece.isAuthorized() << endl;

	vector<Transaction>::const_iterator it;
	const vector<Transaction>& transactions = piece.GetTransactions();
	for (it = transactions.begin(); it != transactions.end(); it++)
	{
		os << *it;
	}

	return os;
}
ifstream& operator>> (ifstream& is, Piece& piece)
{
	string name;
	is >> name;
	piece.SetName(name);

	string authorName;
	is >> authorName;
	piece.SetAuthor(authorName);

	string ownerName;
	is >> ownerName;
	Owner* pOwner = 0;
	if (IGallery::GetOwner(ownerName, pOwner))
		piece.SetOwner(pOwner);

	string size;
	is >> size;
	piece.SetSize(size);

	int year;
	is >> year;
	piece.SetYear(year);

	bool isForSale;
	is >> isForSale;
	piece.SetForSale(isForSale);

	bool isAuthorized;
	is >> isAuthorized;
	piece.SetExpAuthorized(isAuthorized);

	vector<Transaction> transactions;
	Transaction transaction;
	try
	{
		while (is >> transaction)
		{

			transactions.push_back(transaction);
		}
	}
	catch (const exception &e)
	{

	}

	return is;
}