///////////////////////////////////////////////////////////
//  Sculpture.h
//  Implementation of the Class Sculpture
//  Created on:      06-Nov-2013 1:03:15 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Sculpture_H__INCLUDED)
#define Sculpture_H__INCLUDED

#include "Piece.h"

class Sculpture : public Piece
{

public:
	/**
	* Constructor that creates a default Sculpture
	*
	*/
	Sculpture();
	/**
	* Destructor that eliminates the space occupied by an Sculpture
	*
	*/
	~Sculpture();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*@return returns returns true or false whether the class was succesfully intialized
	*/
	bool Initialize(const std::string& filename);

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	void Shutdown();

	/**
	* Function that tells us what type of piece we're dealing with
	*
	*@return returns a string "Sculpture", indicating we're dealing with an Sculpture
	*/
	std::string GetType() const;
	
	/**
	* Pure virtual function that returns the id of the piece depending on it's type
	*
	*@return returns the integer corresponding to the piece's type ID
	*/
	unsigned int GetTypeID() const;

private:
	unsigned int mSculptureID;
	static unsigned int msNumberSculptures;
};
#endif // !defined(Sculpture_H__INCLUDED)
