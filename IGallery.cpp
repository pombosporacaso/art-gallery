///////////////////////////////////////////////////////////
//  IGallery.cpp
//  Implementation of the Class IGallery
//  Created on:      06-Nov-2013 1:03:13 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "IGallery.h"
#include "Gallery.h"
#include "Artist.h"
#include "Reservation.h"
#include "Exposition.h"
#include "Photo.h"
#include "Painting.h"
#include "Handicraft.h"
#include "Sculpture.h"
#include "Transaction.h"
#include "Exceptions.h"
#include "IOGallery.h"

#ifndef iostream
#include <iostream>
#endif // !iostream

#ifndef algorithm
#include <algorithm>
#endif // !algorithm

#ifndef conio
#include <conio.h>
#endif // !conio




using namespace std;

vector<Artist*> IGallery::mArtists;
vector<Client*> IGallery::mClients;
unordered_set<Client, HashClient, HashClient> IGallery::mInactiveClients;
vector<Exposition*> IGallery::mExpositions;
priority_queue<Reservation*> IGallery::mReservations;
Gallery* IGallery::mGallery;
vector<Handicraft*> IGallery::mHandicrafts;
vector<Painting*> IGallery::mPaintings;
vector<Photo*> IGallery::mPhotos;
vector<Sculpture*> IGallery::mSculptures;
IOGallery* IGallery::mInput;
tm IGallery::ts;
int IGallery::mPeriod = 0;

string IGallery::mExpositionsFilename = "IGallery_Expositions.txt";
string IGallery::mReservationsFilename = "IGallery_Reservations.txt";
string IGallery::mArtistsFilename = "IGallery_Artists.txt";
string IGallery::mClientsFilename = "IGallery_Clients.txt";
string IGallery::mHandicraftsFilename = "IGallery_Handicrafts.txt";
string IGallery::mPaintingsFilename = "IGallery_Paintings.txt";
string IGallery::mPhotosFilename = "IGallery_Photos.txt";
string IGallery::mSculpturesFilename = "IGallery_Sculptures.txt";
string IGallery::mGalleryFilename = "IGallery_Gallery.txt";


struct FindArtist
{
	std::string mName;
	FindArtist(std::string name) : mName(name) {}

	bool operator() (Artist* artist)
	{
		return artist->GetName() == mName;
	}
};
struct FindClient
{
	std::string mName;
	FindClient(std::string name) : mName(name) {}

	bool operator() (Client* client)
	{
		return client->GetName() == mName;
	}
};
struct FindExposition
{
	unsigned mID;
	FindExposition(unsigned ID) : mID(ID) {}

	bool operator() (Exposition* exposition)
	{
		return exposition->GetID() == mID;
	}
};
struct FindHandicraft
{
	unsigned int mID;
	FindHandicraft(unsigned int ID) : mID(ID) {}

	bool operator() (Handicraft* handicraft)
	{
		return handicraft->GetID() == mID;
	}
};
struct FindPainting
{
	unsigned int mID;
	FindPainting(unsigned int ID) : mID(ID) {}

	bool operator() (Painting* painting)
	{
		return painting->GetID() == mID;
	}
};
struct FindPhoto
{
	unsigned int mID;
	FindPhoto(unsigned int ID) : mID(ID) {}

	bool operator() (Photo* photo)
	{
		return photo->GetID() == mID;
	}
};
struct FindSculpture
{
	unsigned int mID;
	FindSculpture(unsigned int ID) : mID(ID) {}

	bool operator() (Sculpture* sculpture)
	{
		return sculpture->GetID() == mID;
	}
};
struct FindTransaction
{
	unsigned mID;
	FindTransaction(unsigned ID) : mID(ID) {}

	bool operator() (Transaction* transaction)
	{
		return transaction->GetID() == mID;
	}
};
struct FindOwner
{
	std::string mName;
	FindOwner(std::string name) : mName(name) {}

	bool operator() (Owner* owner)
	{
		return owner->GetName() == mName;
	}
};
struct FindPerson
{
	std::string mName;
	FindPerson(std::string name) : mName(name) {}

	bool operator() (Person* person)
	{
		return person->GetName() == mName;
	}
};
struct FindPiece
{
	std::string mName;
	FindPiece(std::string name) : mName(name) {}

	bool operator() (Piece* piece)
	{
		return piece->GetName() == mName;
	}
};

IGallery::IGallery()
{
	mGallery = 0;
}
IGallery::~IGallery()
{

}

bool IGallery::Initalize()
{
	bool result;

	result = LoadData();
	if (!result)
		return false;

	return true;
}
void IGallery::Shutdown()
{
	bool done = false;
	while (!done)
	{
		try
		{
			SaveData();
			DestroyContainer(mArtists);
			DestroyContainer(mClients);
			DestroyContainer(mExpositions);
			DestroyContainer(mHandicrafts);
			DestroyContainer(mPaintings);
			DestroyContainer(mSculptures);

			if (mGallery)
			{
				delete mGallery;
				mGallery = 0;
			}

			done = true;
		}
		catch (const ESaveException& e)
		{
			cout << endl << e.GetReason();
			_getch();
		}
	}
}

 void IGallery::SaveData()
{
	ofstream outFile;

	// Open Artists file
	outFile.open(mArtistsFilename, ios::out);
	if (!outFile)
		throw ESaveException(mArtistsFilename);
	vector<Artist*>::iterator itArtists;
	for (itArtists = mArtists.begin(); itArtists != mArtists.end(); itArtists++)
	{
		if (*itArtists)
		{
			(*itArtists)->Shutdown();
			string filename; (*itArtists)->GetFilename(filename);
			outFile << filename << endl;
		}
	}
	// Close Artists file
	outFile.close();


	// Open Clients file
	outFile.open(mClientsFilename, ios::out);
	if (!outFile)
		throw ESaveException(mClientsFilename);

	vector<Client*>::iterator itClients;
	for (itClients = mClients.begin(); itClients != mClients.end(); itClients++)
	{
		if (*itClients)
		{
			(*itClients)->Shutdown();
			string filename; (*itClients)->GetFilename(filename);
			outFile << filename << endl;
		}
	}
	// Close Clients file
	outFile.close();

	// Open Expositions file
	outFile.open(mExpositionsFilename, ios::out);
	if (!outFile)
		throw ESaveException(mExpositionsFilename);

	vector<Exposition*>::iterator itExpositions;
	for (itExpositions = mExpositions.begin(); itExpositions != mExpositions.end(); itExpositions++)
	{
		if (*itExpositions)
		{
			(*itExpositions)->Shutdown();
			string filename; (*itExpositions)->GetFilename(filename);
			outFile << filename << endl;
		}
	}
	// Close Expositions file
	outFile.close();

	// Open Reservations file
	outFile.open(mReservationsFilename, ios::out);
	if (!outFile)
		throw ESaveException(mReservationsFilename);

	queue<Reservation*> temp;
	while (!mReservations.empty())
	{
		Reservation* res = mReservations.top();
		temp.push(res);

		if (res)
		{
			res->Shutdown();
			string filename; res->GetFilename(filename);
			outFile << filename << endl;
		}
		mReservations.pop();


	}

	while (!temp.empty())
	{
		Reservation* res = temp.front();
		mReservations.push(res);
		temp.pop();
	}
	// Close Reservations file
	outFile.close();


	// Open Handicrafts file
	outFile.open(mHandicraftsFilename, ios::out);
	if (!outFile)
		throw ESaveException(mHandicraftsFilename);

	std::vector<Handicraft*>::iterator itHandicrafts;
	for (itHandicrafts = mHandicrafts.begin(); itHandicrafts != mHandicrafts.end(); itHandicrafts++)
	{
		if (*itHandicrafts)
		{
			(*itHandicrafts)->Shutdown();
			string filename; (*itHandicrafts)->GetFilename(filename);
			outFile << filename << endl;
		}
	}
	// Close Handicrafts file
	outFile.close();


	// Open Paintings file
	outFile.open(mPaintingsFilename, ios::out);
	if (!outFile)
		throw ESaveException(mPaintingsFilename);

	std::vector<Painting*>::iterator itPaintings;
	for (itPaintings = mPaintings.begin(); itPaintings != mPaintings.end(); itPaintings++)
	{
		if (*itPaintings)
		{
			(*itPaintings)->Shutdown();
			string filename; (*itPaintings)->GetFilename(filename);
			outFile << filename << endl;
		}
	}
	// Close Paintings file
	outFile.close();


	// Open Photos file
	outFile.open(mPhotosFilename, ios::out);
	if (!outFile)
		throw ESaveException(mPhotosFilename);

	std::vector<Photo*>::iterator itPhotos;
	for (itPhotos = mPhotos.begin(); itPhotos != mPhotos.end(); itPhotos++)
	{
		if (*itPhotos)
		{
			(*itPhotos)->Shutdown();
			string filename; (*itPhotos)->GetFilename(filename);
			outFile << filename << endl;
		}
	}
	// Close Photos file
	outFile.close();


	// Open Sculptures file
	outFile.open(mSculpturesFilename, ios::out);
	if (!outFile)
		throw ESaveException(mSculpturesFilename);

	std::vector<Sculpture*>::iterator itSculptures;
	for (itSculptures = mSculptures.begin(); itSculptures != mSculptures.end(); itSculptures++)
	{
		if (*itSculptures)
		{
			(*itSculptures)->Shutdown();
			string filename; (*itSculptures)->GetFilename(filename);
			outFile << filename << endl;
		}
	}
	// Close Sculptures file
	outFile.close();

	if (mGallery)
		mGallery->Shutdown();

	outFile.close();
}
bool IGallery::LoadData()
{
	string filename;
	ifstream inFile;

	// Gallery
	inFile.open(mGalleryFilename, ios::in);
	if (!inFile)
		CreateFile(mGalleryFilename);
	inFile >> filename;
	mGallery = new Gallery();
	mGallery->Initialize(filename);
	inFile.close();

	// Artists
	inFile.open(mArtistsFilename);
	if (!inFile)
		CreateFile(mArtistsFilename);
	while (inFile >> filename)
	{
		Artist* pArtist = new Artist();
		if (!pArtist->Initialize(filename))
			return false;
		mArtists.push_back(pArtist);
	}
	inFile.close();


	// Clients
	inFile.open(mClientsFilename);
	if (!inFile)
		CreateFile(mClientsFilename);
	while (inFile >> filename)
	{
		Client* pClient = new Client();
		if (!pClient->Initialize(filename))
			return false;
		if (pClient->IsClientActive(31536000000))
			mInactiveClients.insert(*pClient);

		mClients.push_back(pClient);
	}
	inFile.close();


	// Handicrafts
	inFile.open(mHandicraftsFilename);
	if (!inFile)
		CreateFile(mHandicraftsFilename);
	while (inFile >> filename)
	{
		Handicraft* pHandicraft = new Handicraft();
		if (!pHandicraft->Initialize(filename))
			return false;
		mHandicrafts.push_back(pHandicraft);
	}
	inFile.close();


	// Paintings
	inFile.open(mPaintingsFilename);
	if (!inFile)
		CreateFile(mPaintingsFilename);
	while (inFile >> filename)
	{
		Painting* pPainting = new Painting();
		if (!pPainting->Initialize(filename))
			return false;
		mPaintings.push_back(pPainting);
	}
	inFile.close();


	// Photos
	inFile.open(mPhotosFilename);
	if (!inFile)
		CreateFile(mPhotosFilename);
	while (inFile >> filename)
	{
		Photo* pPhoto = new Photo();
		if (!pPhoto->Initialize(filename))
			return false;
		mPhotos.push_back(pPhoto);
	}
	inFile.close();


	// Sculptures
	inFile.open(mSculpturesFilename);
	if (!inFile)
		CreateFile(mSculpturesFilename);
	while (inFile >> filename)
	{
		Sculpture* pSculpture = new Sculpture();
		if (!pSculpture->Initialize(filename))
			return false;
		mSculptures.push_back(pSculpture);
	}
	inFile.close();

	// Expositions
	inFile.open(mExpositionsFilename);
	if (!inFile)
		CreateFile(mExpositionsFilename);
	while (inFile >> filename)
	{
		Exposition* pExposition = new Exposition();
		if (!pExposition->Initialize(filename))
			return false;
		mExpositions.push_back(pExposition);
	}
	inFile.close();


	// Reservations
	inFile.open(mReservationsFilename);
	if (!inFile)
		CreateFile(mReservationsFilename);
	while (inFile >> filename)
	{
		Reservation* pRes = new Reservation();
		if (!pRes->Initialize(filename))
			return false;
		mReservations.push(pRes);
	}
	inFile.close();

	return true;
}

bool IGallery::CreateOwner(Owner* pOwner)
{
	if (!pOwner)
		return false;

	string name;
	cout << "Name: ";
	if (!(*mInput >> name))
		return false;

	Owner* pClone = 0;
	if (GetOwner(name, pClone))
		throw EDefault("User already exists");

	cout << "Password: ";
	string password;
	if (!mInput->InputPassword(password))
		return false;

	if (pOwner->GetType() == Client_o)
	{

		string adr;
		cout << "Adress: ";
		if (!(*mInput >> adr))
			return false;
		pOwner->SetAdress(adr);
	}



	pOwner->SetName(name);
	pOwner->SetPassword(password);

	return true;
}
bool IGallery::CreatePiece(Piece* pPiece)
{
	if (!pPiece)
		return false;

	string pieceName;
	cout << "Piece Name: ";
	if (!(*mInput >> pieceName))
		return false;

	string size;
	cout << "Piece size: ";
	if (!Piece::InputSize(size))
		return false;

	int year = 0;
	cout << "Year: ";
	if (!(*mInput >> year))
		return false;


	if (!pPiece->SetSize(size))
		return false;

	pPiece->SetName(pieceName);
	pPiece->SetYear(year);

	return true;
}
bool IGallery::CreateArtist(Artist* &pArtist)
{
	bool result;

	Artist artist;
	result = CreateOwner(&artist);
	if (!result)
		return false;

	pArtist = new Artist(artist);
	mArtists.push_back(pArtist);
	pArtist->Shutdown();

	return true;
}
bool IGallery::CreateClient(Client* &pClient)
{
	bool result;

	Client client;
	result = CreateOwner(&client);
	if (!result)
		return false;

	pClient = new Client(client);
	mClients.push_back(pClient);
	if (!pClient->IsClientActive(12))
		mInactiveClients.insert(*pClient);
	pClient->Shutdown();

	return true;
}
bool IGallery::CreateExposition(Exposition* &pExposition, Owner* owner)
{
	Date BeginDate;
	cout << "Begin Date:\t";
	if (!CreateDate(BeginDate))
		return false;

	Date EndDate;
	cout << "End Date:\t";
	if (!CreateDate(EndDate))
		return false;

	unsigned int iSpace = 0;
	cout << "Space to be reserved:\t";
	if (!(*mInput >> iSpace))
		return false;

	Gallery* pGallery = 0;
	IGallery::GetGallery(pGallery);
	if (iSpace > pGallery->AvailableSpace(BeginDate))
		return false;

	char isThematic;
	cout << "Is it thematic? (y/n) ";
	if (!mInput->InputYesNo(isThematic))
		return false;

	Exposition exposition;
	string theme;
	if (isThematic == 'Y')
	{
		cout << "Theme:\t";
		if (!mInput->InputTheme(theme))
			return false;
		if (owner->GetType() == Gallery_o)
			exposition.AddPieces(theme, GetPieces());
		else
			exposition.AddPieces(theme, owner->GetOwnedPieces());
	}
	else
	{
		theme = "Thematic";
		if (owner->GetType() == Gallery_o)
			exposition.AddPieces(GetPieces());
		else
			exposition.AddPieces(owner->GetOwnedPieces());

	}

	exposition.SetDateBegin(BeginDate);
	exposition.SetDateEnd(EndDate);
	exposition.SetType(theme);
	exposition.SetSpace(iSpace);
	exposition.SetOwner(owner->GetName());
	if (isThematic == 'y')
		exposition.SetThematic(true);
	else
		exposition.SetThematic(false);

	pExposition = new Exposition(exposition);

	return true;
}
bool IGallery::CreateReservation(Reservation* &pReservation, Owner* owner)
{
	Exposition* exp = 0;
	if (!CreateExposition(exp, owner))
		return false;

	Reservation res;
	res.SetDate(exp->GetDateBegin());
	res.SetOwner(owner->GetName());
	res.SetExposition(exp);

	pReservation = new Reservation(res);
	mReservations.push(pReservation);
	return true;

}
bool IGallery::CreateHandicraft(Handicraft* &pHandicraft)
{
	bool result;

	Handicraft handicraft;
	result = CreatePiece(&handicraft);
	if (!result)
		return false;

	pHandicraft = new Handicraft(handicraft);
	mHandicrafts.push_back(pHandicraft);


	return true;
}
bool IGallery::CreatePaiting(Painting* &pPainting)
{
	bool result;

	Painting painting;
	result = CreatePiece(&painting);
	if (!result)
		return false;

	pPainting = new Painting(painting);
	mPaintings.push_back(pPainting);


	return true;
}
bool IGallery::CreatePhoto(Photo* &pPhoto)
{
	bool result;

	Photo photo;
	result = CreatePiece(&photo);
	if (!result)
		return false;

	pPhoto = new Photo(photo);
	mPhotos.push_back(pPhoto);


	return true;
}
bool IGallery::CreateSculpture(Sculpture* &pSculpture)
{
	bool result;

	Sculpture sculpture;
	result = CreatePiece(&sculpture);
	if (!result)
		return false;

	pSculpture = new Sculpture(sculpture);
	mSculptures.push_back(pSculpture);


	return true;
}
bool IGallery::CreateDate(Date& date)
{
	if (!Date::InputDate(date))
		return false;

	return true;
}
bool IGallery::CreateTransaction(Transaction& pTransaction)
{
	double price;
	cout << "Price:\t";
	if (!(*mInput >> price))
		return false;


	Date date;
	if (!Date::InputDate(date))
		return false;

	pTransaction.SetPrice(price);
	pTransaction.SetDate(date);

	return true;
}

bool IGallery::GetArtist(const string& name, Artist* &pArtist)
{
	vector<Artist*>::iterator it = find_if(mArtists.begin(), mArtists.end(), FindArtist(name));
	if (it == mArtists.end())
		return false;

	pArtist = *it;

	return true;
}
bool IGallery::GetClient(const string& name, Client* &pClient)
{
	vector<Client*>::iterator it = find_if(mClients.begin(), mClients.end(), FindClient(name));

	if (it == mClients.end())
		return false;

	pClient = *it;

	return true;
}
bool IGallery::GetInactiveClient(const string& name, Client* &pClient)
{
	
	pClient->SetName(name);
	unordered_set<Client, HashClient, HashClient>::const_iterator it1;
	it1 = mInactiveClients.find(*pClient);

	if (it1 == mInactiveClients.end())
		return false;


	*pClient = *it1;

	return true;
}
bool IGallery::GetOwner(const string& name, Owner* &pOwner)
{
	bool result;

	Person* pPerson = 0;
	result = GetPerson(name, pPerson);
	if (result)
	{
		pOwner = pPerson;
		return true;
	}

	if (mGallery->GetName() == name)
	{
		pOwner = mGallery;
		return true;
	}

	return false;
}
bool IGallery::GetExposition(unsigned int ID, Exposition* &pExposition)
{
	vector<Exposition*>::iterator it = find_if(mExpositions.begin(), mExpositions.end(), FindExposition(ID));
	if (it == mExpositions.end())
		return false;

	pExposition = *it;

	return true;
}
bool IGallery::GetReservation(unsigned int ID, Reservation* &pReservation)
{
	queue<Reservation*> temp;
	while (!mExpositions.empty())
	{
		Reservation* res = mReservations.top();
		temp.push(res);
		mReservations.pop();
		if (res->GetID() == ID)
		{
			while (!temp.empty())
			{
				Reservation* res = temp.front();
				mReservations.push(res);
				temp.pop();
			}
			pReservation = res;
			return true;

		}
	}

	while (!temp.empty())
	{
		Reservation* res = temp.front();
		mReservations.push(res);
		temp.pop();
	}

	return false;
}
bool IGallery::GetPiece(unsigned int ID, Piece* &pPiece)
{
	bool result;

	Handicraft* pHandicraft = 0;
	result = GetHandicraft(ID, pHandicraft);
	if (result)
	{
		pPiece = pHandicraft;
		return true;
	}

	Painting* pPainting = 0;
	result = GetPainting(ID, pPainting);
	if (result)
	{
		pPiece = pPainting;
		return true;
	}

	Photo* pPhoto = 0;
	result = GetPhoto(ID, pPhoto);
	if (result)
	{
		pPiece = pPhoto;
		return true;
	}

	Sculpture* pSculpture = 0;
	result = GetSculpture(ID, pSculpture);
	if (result)
	{
		pPiece = pSculpture;
		return true;
	}

	return false;
}
bool IGallery::GetPiece(std::string name, Piece* &pPiece)
{
	vector<Handicraft*>::iterator hIt;
	hIt = find_if(mHandicrafts.begin(), mHandicrafts.end(), [&](Handicraft* h)
	{
		return h->GetName() == name;
	});
	if (hIt != mHandicrafts.end())
	{
		pPiece = *hIt;
		return true;
	}

	vector<Painting*>::iterator pIt;
	pIt = find_if(mPaintings.begin(), mPaintings.end(), [&](Painting* p)
	{
		return p->GetName() == name;
	});
	if (pIt != mPaintings.end())
	{
		pPiece = *pIt;
		return true;
	}

	vector<Photo*>::iterator phIt;
	phIt = find_if(mPhotos.begin(), mPhotos.end(), [&](Photo* ph)
	{
		return ph->GetName() == name;
	});
	if (phIt != mPhotos.end())
	{
		pPiece = *phIt;
		return true;
	}

	vector<Sculpture*>::iterator sIt;
	sIt = find_if(mSculptures.begin(), mSculptures.end(), [&](Sculpture* s)
	{
		return s->GetName() == name;
	});
	if (sIt != mSculptures.end())
	{
		pPiece = *sIt;
		return true;
	}

	return false;
}
bool IGallery::GetHandicraft(unsigned int ID, Handicraft* &pHandicraft)
{
	vector<Handicraft*>::iterator it = find_if(mHandicrafts.begin(), mHandicrafts.end(), FindHandicraft(ID));
	if (it == mHandicrafts.end())
		return false;

	pHandicraft = *it;

	return true;
}
bool IGallery::GetPainting(unsigned int ID, Painting* &pPainting)
{
	vector<Painting*>::iterator it = find_if(mPaintings.begin(), mPaintings.end(), FindPainting(ID));
	if (it == mPaintings.end())
		return false;

	pPainting = *it;

	return true;
}
bool IGallery::GetPhoto(unsigned int ID, Photo* &pPhoto)
{
	vector<Photo*>::iterator it = find_if(mPhotos.begin(), mPhotos.end(), FindPhoto(ID));
	if (it == mPhotos.end())
		return false;

	pPhoto = *it;

	return true;
}
bool IGallery::GetSculpture(unsigned int ID, Sculpture* &pSculpture)
{
	vector<Sculpture*>::iterator it = find_if(mSculptures.begin(), mSculptures.end(), FindSculpture(ID));
	if (it == mSculptures.end())
		return false;

	pSculpture = *it;

	return true;
}
bool IGallery::GetPerson(const string& name, Person* &pPerson)
{
	bool result;

	Artist* pArtist = 0;
	result = GetArtist(name, pArtist);
	if (result)
	{
		pPerson = pArtist;
		return true;
	}

	Client* pClient = 0;
	result = GetClient(name, pClient);
	if (result)
	{
		pPerson = pClient;
		return true;
	}

	return false;
}
void IGallery::GetGallery(Gallery* &pGallery)
{
	pGallery = mGallery;
}
int IGallery::GetPeriod()
{
	return mPeriod;
}

void IGallery::SetExpositions(std::vector<Exposition*> &pExposition)
{
	mExpositions = pExposition;
}
void IGallery::SetReservations(std::priority_queue<Reservation*> &pReservation)
{
	mReservations = pReservation;
}
void IGallery::SetPeriod(int period)
{
	mPeriod += period;
}

bool IGallery::DeleteArtist(const string& name)
{
	vector<Artist*>::iterator it;
	it = find_if(mArtists.begin(), mArtists.end(), FindArtist(name));
	if (it == mArtists.end())
		return false;

	(*it)->Shutdown();
	delete *it;
	*it = 0;
	mArtists.erase(it);

	return true;
}
bool IGallery::DeleteClient(const string& name)
{
	vector<Client*>::iterator it;
	it = find_if(mClients.begin(), mClients.end(), FindClient(name));
	if (it == mClients.end())
		return false;

	unordered_set<Client, HashClient, HashClient>::const_iterator it1;
	it1 = mInactiveClients.find(**it);
	if (it1 != mInactiveClients.end())
	{
		mInactiveClients.erase(it1);
	}
	(*it)->Shutdown();
	delete *it;
	*it = 0;
	mClients.erase(it);

	return true;
}
bool IGallery::DeleteInactiveClient(const string& name)
{
	Client* cli1 = 0;
	if (!GetClient(name, cli1))
		return false;

	unordered_set<Client, HashClient, HashClient>::const_iterator it1;
	it1 = mInactiveClients.find(*cli1);
	if (it1 == mInactiveClients.end())
		return false;

	mInactiveClients.erase(it1);
	return true;
}
bool IGallery::DeleteHandicraft(unsigned int ID)
{
	vector<Handicraft*>::iterator it;
	it = find_if(mHandicrafts.begin(), mHandicrafts.end(), FindHandicraft(ID));
	if (it == mHandicrafts.end())
		return false;

	(*it)->Shutdown();
	delete *it;
	*it = 0;
	mHandicrafts.erase(it);

	return true;
}
bool IGallery::DeletePaintings(unsigned int ID)
{
	vector<Painting*>::iterator it;
	it = find_if(mPaintings.begin(), mPaintings.end(), FindPainting(ID));
	if (it == mPaintings.end())
		return false;

	(*it)->Shutdown();
	delete *it;
	*it = 0;
	mPaintings.erase(it);

	return true;
}
bool IGallery::DeletePhoto(unsigned int ID)
{
	vector<Photo*>::iterator it;
	it = find_if(mPhotos.begin(), mPhotos.end(), FindPhoto(ID));
	if (it == mPhotos.end())
		return false;

	(*it)->Shutdown();
	delete *it;
	*it = 0;
	mPhotos.erase(it);

	return true;
}
bool IGallery::DeleteSculpture(unsigned int ID)
{
	vector<Sculpture*>::iterator it;
	it = find_if(mSculptures.begin(), mSculptures.end(), FindSculpture(ID));
	if (it == mSculptures.end())
		return false;

	(*it)->Shutdown();
	delete *it;
	*it = 0;
	mSculptures.erase(it);

	return true;
}
bool IGallery::DeleteExposition(unsigned int ID)
{
	vector<Exposition*>::iterator it;
	it = find_if(mExpositions.begin(), mExpositions.end(), FindExposition(ID));
	if (it == mExpositions.end())
		return false;

	(*it)->Shutdown();
	delete *it;
	*it = 0;
	mExpositions.erase(it);

	return true;
}
bool IGallery::DeleteReservation(unsigned int ID)
{
	queue<Reservation*> temp;

	while (!mReservations.empty())
	{
		Reservation* res = mReservations.top();

		mReservations.pop();
		if (res->GetID() == ID)
		{
			while (!temp.empty())
			{
				Reservation* res = temp.front();
				mReservations.push(res);
				temp.pop();
			}
			delete res;
			res = 0;
			return true;
		}
		else
			temp.push(res);
	}

	while (!temp.empty())
	{
		Reservation* res = temp.front();
		mReservations.push(res);
		temp.pop();
	}

	return false;
}

template <class T>
void IGallery::DestroyContainer(vector<T*>& container)
{
	typename vector<T*>::iterator current;

	for (current = container.begin(); current != container.end(); current++)
	{
		if (*current)
		{
			delete *current;
			*current = 0;
		}
	}
}

vector<Piece*> IGallery::GetPieces()
{
	vector<Piece*> gPieces;
	/*int size = 0;

	size = mHandicrafts.size() + mPaintings.size() + mPhotos.size() + mSculptures.size();
	gPieces.resize(size);
	*/
	for (unsigned int i = 0; i < mHandicrafts.size(); i++)
	{
		gPieces.push_back(mHandicrafts[i]);
	}

	for (unsigned int i = 0; i < mPaintings.size(); i++)
	{
		gPieces.push_back(mPaintings[i]);
	}

	for (unsigned int i = 0; i < mSculptures.size(); i++)
	{
		gPieces.push_back(mSculptures[i]);
	}

	for (unsigned int i = 0; i < mPhotos.size(); i++)
	{
		gPieces.push_back(mPhotos[i]);
	}

	return gPieces;

}
vector<Exposition*> IGallery::GetExpositions()
{
	return mExpositions;
}
priority_queue<Reservation*> IGallery::GetReservations()
{
	return mReservations;
}
vector<Owner*> IGallery::GetOwners()
{
	vector<Owner*> owners;

	for (unsigned int i = 0; i < mArtists.size(); i++)
		owners.push_back(mArtists[i]);

	for (unsigned int i = 0; i < mClients.size(); i++)
		owners.push_back(mClients[i]);

	return owners;
}

bool IGallery::CreateFile(const std::string& filename)
{
	ofstream outFile;
	outFile.open(filename);
	if (!outFile)
		return false;

	outFile.close();

	return true;
}

bool IGallery::AddFilename(const std::string &filenameType, std::string &filename)
{
	ofstream outFile;
	outFile.open(filenameType, ios::app);
	if (!outFile)
		return false;

	outFile << filename << endl;

	outFile.close();

	return true;
}

void IGallery::UpdateTime()
{
	time_t now;
	time(&now);
	ts = *localtime(&now);
}