///////////////////////////////////////////////////////////
//  Transaction.cpp
//  Implementation of the Class Transaction
//  Created on:      06-Nov-2013 1:03:16 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Transaction.h"
#include "Owner.h"
#include "IGallery.h"
#include "Exceptions.h"

using namespace std;

#ifndef sstream
#include <sstream>
#endif // !sstream

unsigned int Transaction::msNumberTransactions = 0;

Transaction::Transaction()
{
	msNumberTransactions++;
	mID = msNumberTransactions;
}
Transaction::~Transaction()
{
	msNumberTransactions--;
}

const Owner& Transaction::GetBuyer() const
{
	return *mBuyer;
}
const Date& Transaction::GetDate() const
{
	return mDate;
}
unsigned int Transaction::GetID() const
{

	return mID;
}
double Transaction::GetPrice() const
{

	return mPrice;
}
const Owner& Transaction::GetSeller() const
{
	return *mSeller;
}

void Transaction::SetBuyer(Owner& buyer)
{
	mBuyer = &buyer;
}
void Transaction::SetDate(const Date& date)
{
	mDate = date;
}
void Transaction::SetPrice(double price)
{
	mPrice = price;
}
void Transaction::SetSeller(Owner& seller)
{
	mSeller = &seller;
}

bool Transaction::operator==(const Transaction& transaction) const
{
	return transaction.GetID() == mID;
}

ostream& operator<<(ostream& os, const Transaction& transaction)
{
	os << "Transaction number " << transaction.GetID() << "\nBuyer: " << transaction.GetBuyer() << "\n" << "Seller: " << transaction.GetSeller() << "\nPrice: " << transaction.GetPrice() << "\nDate: " << transaction.GetDate();
	return os;
}
ofstream& operator<< (ofstream& os, const Transaction& transaction)
{
	os << transaction.GetPrice() << endl;
	os << transaction.GetDate();
	os << transaction.GetBuyer().GetName();
	os << transaction.GetSeller().GetName();

	return os;
}
ifstream& operator>> (ifstream& is, Transaction& transaction)
{
	unsigned int price;
	is >> price;
	transaction.SetPrice(price);

	Date date;
	is >> date;
	transaction.SetDate(date);

	string buyerName;
	is >> buyerName;
	Owner* buyer = 0;
	if (IGallery::GetOwner(buyerName, buyer))
		transaction.SetBuyer(*buyer);

	string sellerName;
	is >> sellerName;
	Owner* seller = 0;
	if (IGallery::GetOwner(sellerName, seller))
		transaction.SetSeller(*seller);

	return is;
}