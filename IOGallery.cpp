#include "IOGallery.h"

#ifndef conio
#include <conio.h>
#endif

#ifndef iostream
#include <iostream>
#endif

using namespace std;

bool IOGallery::mValid = true;

IOGallery::IOGallery()
{
}

IOGallery& IOGallery::operator>>(std::string& input)
{
	string str_tmp;
	stringstream ss;

	do
	{
		if (!GetInput('s', ss))
		{
			mValid = false;
			break;
		}

		while (ss >> ws >> str_tmp >> ws)		// Extracting each word, without spaces
			input += str_tmp + ' ';				// Adding to final string + space

		input = input.substr(0, input.size() - 1);
	} while (input.size() == 0);

	return *this;
}
IOGallery& IOGallery::operator>>(int &input)
{
	stringstream ss;

	input = -1;
	do
	{
		if (!GetInput('d', ss))
		{
			mValid = false;
			break;
		}

		ss >> input;

	} while (input == -1);

	return *this;
}
IOGallery& IOGallery::operator>>(unsigned int &input)
{
	stringstream ss;

	input = 0;
	do
	{
		if (!GetInput('d', ss))
		{
			mValid = false;
			break;
		}

		ss >> input;

	} while (input == 0);

	return *this;
}
IOGallery& IOGallery::operator>>(double &input)
{
	stringstream ss;

	input = 0;
	do
	{
		if (!GetInput('f', ss))
		{
			mValid = false;
			break;
		}

		ss >> input;

	} while (input == 0);

	return *this;
}
bool IOGallery::InputYesNo(char &input)
{
	input = 'N';

	while (true)
	{
		// Waiting for key press
		int key = _getch();

		// Drive out arrow keys
		if (key == -32)
		{
			_getch();
			continue;
		}

		if (key == 'y' || key == 'n') // Converting 'y' or 'n' to upper case
			key -= 0x20;

		if ((key == 'Y' || key == 'N') && input == 'N') // M or F
		{
			input = key;		// If the input is valid
			cout << input;		// Printing to the screen
		}

		// Backspace
		else if (key == '\b' && input != 'N')
		{
			input = 'N';		// Resetting gender
			cout << "\b \b";	// Erasing char from the screen
		}

		// Enter
		else if (key == 0xD /*&& input != 'N'*/)
			break;

		// Escape
		else if (key == 0x1B)
			return false;
	}

	// '\n' corresponding to the last enter
	cout << endl;

	return true;
}
bool IOGallery::InputPassword(std::string& input)
{
	string str_tmp;
	stringstream ss;

	do
	{
		if (!GetInput('p', ss))
			return false;

		while (ss >> ws >> str_tmp >> ws)		// Extracting each word, without spaces
			input += str_tmp + ' ';				// Adding to final string + space

		input = input.substr(0, input.size() - 1);
	} while (input.size() == 0);

	return true;
}
bool IOGallery::IsValidKey(char key, char type)
{
	// Strings
	if (type == 's')
		return ((key >= 'A' && key <= 'Z')		// A-Z
		|| (key >= 'a' && key <= 'z')			// a-z
		|| key == 0x20);						// space
	// Numbers
	else if (type == 'd' || type == 'f')
		return ((key >= '0' && key <= '9'));	// 0-9  
	// Both s, d and f
	else
		return ((key >= '0' && key <= '9')		// 0-9
		|| (key >= 'A' && key <= 'Z')			// A-Z
		|| (key >= 'a' && key <= 'z')			// a-z
		|| key == 0x20);						// space
}
bool IOGallery::GetInput(char type, stringstream& ss)
{
	int size = 0;
	bool dot = false;

	while (true) // While enter is not pressed and the input is not valid
	{
		// Waiting for key press
		char key = _getch();

		// Drive out arrow keys
		if (key == -32)
		{
			_getch();
			continue;
		}



		if (type == 'f' && key == '.')
		{
			ss << key;
			dot = true;
			size++;
			cout << key;
		}

		// If the key pressed corresponds to a letter or space
		if (IsValidKey(key, type))
		{
			ss << key;
			size++;					// Incrementing input size
			if (type == 'p')
				cout << '*';
			else
				cout << key;			// Printing on screen
		}

		// If the key is backspace
		else if (key == '\b' && size != 0)
		{
			string data;
			getline(ss, data);

			if (data[data.size() - 1] == '.')
				dot = false;

			ss = stringstream(data.substr(0, data.size() - 1));
			size--;											// Decrementing input size
			cout << "\b \b";								// Erase char on the screen
		}

		// If the key is enter
		else if (key == 0x0D && size != 0)
			break;

		// Escape
		else if (key == 0x1B)
			return false;
	}

	cout << endl; // '\n' which corresponds to the last enter

	if (dot == false && type == 'f')
	{
		ss.put('.');
		ss.put('0');
	}

	return true;
}
bool IOGallery::InputOwner(std::string &input)
{
	char key;
	bool done = false;

	while (true)
	{
		// Waiting for a key press
		key = _getch();

		// Drive out arrow keys
		if (key == -32)
		{
			_getch();
			continue;
		}

		// Capital letter
		if (key >= 'a')
			key -= 0x20;

		// Escape
		if (key == 0x1B)
			return false;

		// Enter
		if (key == '\r' && done)
			break;

		// Backspace
		if (key == '\b' && input.size() != 0)
		{
			for (unsigned i = 0; i < input.size(); i++)
			{
				cout << "\b \b";
			}

			input = "";
			done = false;
		}

		if (input.size() == 0)
		{
			switch (key)
			{
			case 'A':
				cout << "Artist";
				input = "Artist";
				done = true;
				break;
			case 'C':
				cout << "Client";
				input = "Client";
				done = true;
				break;
			}
		}
	}

	cout << endl;
}

IOGallery::operator bool()
{
	if (!mValid)
	{
		mValid = true;
		return false;
	}

	return true;
}

bool IOGallery::InputTheme(string &input)
{
	char key;
	bool done = false;

	while (true)
	{
		// Waiting for a key press
		key = _getch();

		// Drive out arrow keys
		if (key == -32)
		{
			_getch();
			continue;
		}

		// Capital letter
		if (key >= 'a')
			key -= 0x20;

		// Escape
		if (key == 0x1B)
			return false;

		// Enter
		if (key == '\r' && done)
			break;

		// Backspace
		if (key == '\b' && input.size() != 0)
		{
			for (unsigned i = 0; i < input.size(); i++)
			{
				cout << "\b \b";
			}

			input = "";
			done = false;
		}

		if (input.size() == 0)
		{
			switch (key)
			{
			case 'S':
				cout << "Sculptures";
				input = "Sculptures";
				done = true;
				break;
			case 'H':
				cout << "Handicrafts";
				input = "Handicrafts";
				done = true;
				break;
			case 'P':
				cout << 'P';
				input = "P";
				break;
			}
		}
		else if (input.size() == 1)
		{
			switch (key)
			{
			case 'A':
				cout << "aintings";
				input = "Paintings";
				done = true;
				break;
			case 'H':
				cout << "hotos";
				input = "Photos";
				done = true;
				break;
			}
		}
	}

	return true;
}