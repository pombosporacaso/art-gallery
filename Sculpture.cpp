///////////////////////////////////////////////////////////
//  Sculpture.cpp
//  Implementation of the Class Sculpture
//  Created on:      06-Nov-2013 1:03:15 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Sculpture.h"
#include "Exceptions.h"
#include "IGallery.h"

using namespace std;

unsigned int Sculpture::msNumberSculptures = 0;

Sculpture::Sculpture()
{
	msNumberSculptures++;
	mSculptureID = msNumberSculptures;
}
Sculpture::~Sculpture()
{
	msNumberSculptures--;
}

bool Sculpture::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Sculpture::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);
	IGallery::AddFilename(IGallery::mSculpturesFilename, filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

string Sculpture::GetType() const
{
	return "Sculpture";
}
unsigned int Sculpture::GetTypeID() const
{
	return mSculptureID;
}