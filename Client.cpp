///////////////////////////////////////////////////////////
//  Client.cpp
//  Implementation of the Class Client
//  Created on:      06-Nov-2013 1:03:12 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Client.h"
#include "Exceptions.h"
#include "IGallery.h"

#ifndef fstream
#include <fstream>
#endif // !fstream

#ifndef sstream
#include <sstream>
#endif // !sstream

using namespace std;

unsigned int Client::msNumberClients = 0;

Client::Client()
{
	msNumberClients++;
	mClientID = msNumberClients;
}
Client::~Client()
{
	msNumberClients--;
}

bool Client::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Client::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);
	IGallery::AddFilename(IGallery::mClientsFilename, filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

bool Client::SaveData(const string& filename) const
{
	ofstream outFile;
	outFile.open(filename, ios::out);
	if (!outFile)
		return false;

	outFile << *this;

	outFile.close();

	return true;
}
bool Client::LoadData(const string& filename)
{
	ifstream inFile;
	inFile.open(filename, ios::in);
	if (!inFile)
		return false;

	inFile >> *this;

	inFile.close();

	return true;
}
void Client::GetFilename(std::string& filename) const
{
	stringstream ss;

	ss << "Client_" << mClientID << ".txt";

	ss >> filename;
}

unsigned int Client::GetID() const
{
	return mClientID;
}
owner_t Client::GetType() const
{
	return Client_o;
}

