#include "System.h"
#include "IOGallery.h"
#include "IGallery.h"

#ifndef conio
#include <conio.h>
#endif // !conio.h

#ifndef fstream
#include <fstream>
#endif // !fstream

using namespace std;

int main()
{
	bool result;
	System system = System();

	
	result = system.Initialize();
	if (!result)
		return 0;

IGallery::UpdateTime();
	int binactiveTime = IGallery::ts.tm_mday + IGallery::ts.tm_mday*IGallery::ts.tm_mon;
	IGallery::SetPeriod(binactiveTime);

	system.Run();

	system.Shutdown();

	return 0;
}