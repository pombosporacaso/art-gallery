///////////////////////////////////////////////////////////
//  Date.cpp
//  Implementation of the Class Date
//  Created on:      06-Nov-2013 1:03:12 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Date.h"
#include "IGallery.h"
#include "IOGallery.h"

using namespace std;

Date::Date()
{
}
Date::~Date()
{
}

unsigned int Date::GetDay() const
{
	return mDay;
}
unsigned int Date::GetHours() const
{
	return mHours;
}
unsigned int Date::GetMinutes() const
{
	return mMinutes;
}
unsigned int Date::GetMonth() const
{
	return mMonth;
}
int Date::GetYear() const
{
	return mYear;
}

bool Date::SetDay(unsigned int day){

	if (day <= 31 && day > 0)
	{
		mDay = day;
		return true;
	}

	return false;
}
bool Date::SetHours(unsigned int hours){

	if (hours < 24 && hours >= 0)
	{
		mHours = hours;
		return true;
	}

	return false;
}
bool Date::SetMinutes(unsigned int minutes){

	if (minutes <= 59 && minutes >= 0)
	{
		mMinutes = minutes;
		return true;
	}
	return false;
}
bool Date::SetMonth(unsigned int month)
{
	if (month <= 12 && month > 0)
	{
		mMonth = month;
		return true;
	}
	return false;
}
void Date::SetYear(int year)
{
	mYear = year;
}

bool Date::InputDate(Date& date)
{
	unsigned int day;
	cout << "Day:\t";
	if (!(*IGallery::mInput >> day))
		return false;
	
	unsigned int month;
	cout << "Month:\t";
	if (!(*IGallery::mInput >> month))
		return false;

	unsigned int year;
	cout << "Year:\t";
	if (!(*IGallery::mInput >> year))
		return false;

	unsigned int hours;
	cout << "Hours:\t";
	if (!(*IGallery::mInput >> hours))
		return false;

	unsigned int minutes;
	cout << "Minutes:\t";
	if (!(*IGallery::mInput >> minutes))
		return false;

	date.SetDay(day);
	date.SetHours(hours);
	date.SetMinutes(minutes);
	date.SetMonth(month);
	date.SetYear(year);
	
	return true;
}

ostream& operator<< (ostream& os, const Date& date)
{
	os << "\n\t Minutes:\t" << date.GetMinutes() << endl;
	os << "\n\t Hours:\t" << date.GetHours() << endl;
	os << "\n\t Day:\t\t" << date.GetDay() << endl;
	os << "\n\t Month:\t" << date.GetMonth() << endl;
	os << "\n\t Year:\t" << date.GetYear() << endl;

	return os;
}

std::ofstream& operator<< (std::ofstream& os, const Date& date)
{
	os << date.GetMinutes() << ",";
	os << date.GetHours() << ",";
	os << date.GetDay() << ",";
	os << date.GetMonth() << ",";
	os << date.GetYear() << endl;

	return os;
}
std::ifstream& operator>> (std::ifstream& is, Date& date)
{
	string data;

	getline(is, data, ',');
	date.SetMinutes(stoi(data));

	getline(is, data, ',');
	date.SetHours(stoi(data));

	getline(is, data, ',');
	date.SetDay(stoi(data));

	getline(is, data, ',');
	date.SetMonth(stoi(data));

	getline(is, data, '\n');
	date.SetYear(stoi(data));

	return is;
}

bool Date::operator==(const Date& date) const
{
	return (mDay == date.mDay && mHours == date.mHours && mMinutes == date.mMinutes && mYear == date.mYear && mMonth == date.mMonth);
}