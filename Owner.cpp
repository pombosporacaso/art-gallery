///////////////////////////////////////////////////////////
//  Owner.cpp
//  Implementation of the Class Owner
//  Created on:      06-Nov-2013 1:03:14 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Owner.h"
#include "Piece.h"
#include "IGallery.h"

#ifndef algorithm
#include <algorithm>
#endif // !algorithm

#ifndef sstream
#include <sstream>
#endif // !sstream



using namespace std;

Owner::Owner()
{
	mTransactions = 0;
	mInactiveTime = 0;
	mAcumValue = 0;
	mAdress = "";
}
Owner::~Owner()
{
}

bool Owner::AddOwnedPiece(unsigned int ID)
{
	vector<Piece*>::iterator it;

	it = find_if(mOwnedPieces.begin(), mOwnedPieces.end(), [&](Piece* p)
	{
		return p->GetID() == ID;
	});

	if (it != mOwnedPieces.end())
		return false;

	Piece* pPiece = 0;
	IGallery::GetPiece(ID, pPiece);
	pPiece->SetOwner(this);

	mOwnedPieces.push_back(pPiece);

	return true;
}

bool Owner::BuyPiece(unsigned int ID, int price)
{
	vector<Piece*>::iterator it;

	it = find_if(mOwnedPieces.begin(), mOwnedPieces.end(), [&](Piece* p)
	{
		return p->GetID() == ID;
	});

	if (it != mOwnedPieces.end())
		return false;

	if (!(*it)->IsForSale())
		return false;


	if (!AddOwnedPiece(ID))
		return false;

	mTransactions++;
	mAcumValue += price;

	return true;

}

const string& Owner::GetName() const
{
	return mName;
}
const vector<Piece*>& Owner::GetOwnedPieces() const
{
	return mOwnedPieces;
}

const string& Owner::GetPassword() const
{
	return mPassword;
}

int Owner::GetAcumulatedValue() const
{
	return mAcumValue;
}

void Owner::SetTransactions(int trans)
{
	mTransactions = trans;
}
void Owner::IncTransactions()
{
	mTransactions++;
}

void Owner::IntAcumulatedValue(int value)
{
	mAcumValue += value;
}
void Owner::SetAcumulatedValue(int value)
{
	mAcumValue = value;
}
bool Owner::RemoveOwnedPiece(unsigned int ID)
{
	vector<Piece*>::iterator it;
	for (it = mOwnedPieces.begin(); it != mOwnedPieces.end(); it++)
	{
		if ((*it)->GetID() == ID)
		{
			mOwnedPieces.erase(it);
			return true;
		}
	}

	return false;
}

void Owner::SetName(const string& name)
{
	mName = name;
}
bool Owner::SetOwnedPiece(unsigned int index, Piece* piece)
{
	if (mOwnedPieces[index])
	{
		mOwnedPieces[index] = piece;
		return true;
	}
	return false;
}

void Owner::SetPassword(const std::string& pass)
{
	mPassword = pass;
}

int Owner::GetTransactions() const
{
	return mTransactions;
}

string Owner::GetAdress() const
{
	return mAdress;
}
void Owner::SetAdress(const string& adr)
{
	mAdress = adr;
}

int Owner::GetInactiveTime() const
{
	return mInactiveTime;
}

void Owner::SetInactiveTime(int time)
{
	mInactiveTime += time;
}

bool Owner::IsClientActive(int period)
{
	return mInactiveTime < period;
}

bool Owner::operator==(const Owner& owner) const
{
	return mName == owner.GetName();
}

bool Owner::operator<(const Owner& owner)
{
	if (mTransactions == owner.mTransactions)
	{
		return mAcumValue < owner.mAcumValue;
	}

	return mTransactions < owner.mTransactions;
}
ostream& operator<<(ostream& os, const Owner& owner)
{
	os << "\n\t " << owner.GetType() << '\n';
	os << "\n\t Name: \t" << owner.GetName() << '\n';
	os << "\n\t Pieces: \n\t\t";

	const vector<Piece*>& pieces = owner.GetOwnedPieces();
	if (pieces.empty())
		os << "None\n\n";
	else
	{
		for (unsigned i = 0; i < pieces.size(); i++)
		{
			os << pieces[i]->GetID() << "\t";

			if (i % 4 == 0)
				os << endl;
		}
	}

	return os;
}
ofstream& operator<< (ofstream& os, const Owner& owner)
{
	vector<Piece*>::const_iterator it;

	os << owner.GetName() << endl;
	os << owner.GetPassword() << endl;
	os << owner.GetTransactions() << endl;
	os << owner.GetAcumulatedValue() << endl;
	if (owner.GetType() == Client_o)
	{
		os << owner.GetAdress() << endl;
		os << owner.GetInactiveTime() << endl;
	}


	const vector<Piece*>& ownedPieces = owner.GetOwnedPieces();
	for (it = ownedPieces.begin(); it != ownedPieces.end(); it++)
		os << (*it)->GetID() << ',';
	os << endl;

	return os;
}
ifstream& operator>> (ifstream& is, Owner& owner)
{
	string data;
	is >> data;
	owner.SetName(data);

	is >> data;
	owner.SetPassword(data);
	
	int num;
	is >> num;
	owner.SetTransactions(num);

	is >> num;
	owner.SetAcumulatedValue(num);
	
	if (owner.GetType() == Client_o)
	{
		is >> data;
		owner.SetAdress(data);
		int time;
		is >> time;
		owner.SetInactiveTime(time);
	}

	getline(is, data, '\n');
	stringstream ss(data);

	unsigned int ID;
	while (getline(ss, data, ','))
	{
		try
		{
			ID = stoi(data);
			owner.AddOwnedPiece(ID);
		}
		catch (const std::exception &e){}
	}

	return is;
}