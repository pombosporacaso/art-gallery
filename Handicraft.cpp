///////////////////////////////////////////////////////////
//  Handicraft.cpp
//  Implementation of the Class Handicraft
//  Created on:      06-Nov-2013 1:03:13 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#include "Handicraft.h"
#include "Exceptions.h"
#include "IGallery.h"

using namespace std;

unsigned int Handicraft::msNumberHandicrafts = 0;

Handicraft::Handicraft()
{
	msNumberHandicrafts++;
	mHandicraftID = msNumberHandicrafts;
}
Handicraft::~Handicraft()
{
	msNumberHandicrafts--;
}

bool Handicraft::Initialize(const string& filename)
{
	bool result;

	result = LoadData(filename);
	if (!result)
		return false;

	return true;
}
void Handicraft::Shutdown()
{
	bool result;

	string filename;
	GetFilename(filename);
	IGallery::AddFilename(IGallery::mHandicraftsFilename, filename);

	result = SaveData(filename);
	if (!result)
		throw ESaveException(filename);
}

string Handicraft::GetType() const
{
	return "Handicraft";
}
unsigned int Handicraft::GetTypeID() const
{
	return mHandicraftID;
}