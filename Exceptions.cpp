#include "Exceptions.h"

using namespace std;

EGalleryException::EGalleryException()
{
}
ostream& operator<<(ostream& os, const EGalleryException& e)
{
	os << e.GetReason() << std::endl;

	return os;
}

ESaveException::ESaveException(const std::string filename) : mFilename(filename), EGalleryException()
{
}
string ESaveException::GetReason() const
{
	return "\t Couldn't save file " + mFilename + '\n' + "\t Please close the file to continue\n";
}

EWrongID::EWrongID()
{
}
string EWrongID::GetReason() const
{
	return "Piece not found";
}

EEmpty::EEmpty()
{
}
string EEmpty::GetReason() const
{
	return "Nothing was found";
}

EWrongUsername::EWrongUsername(bool isUsername)
{
	mIsUsername = isUsername;
}
string EWrongUsername::GetReason() const
{
	if (mIsUsername)
		return "Wrong username";

	return "Wrong password";
}

EDefault::EDefault(const std::string &reason)
{
	mReason = reason;
}
string EDefault::GetReason() const
{
	return mReason;
}