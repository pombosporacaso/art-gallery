///////////////////////////////////////////////////////////
//  Handicraft.h
//  Implementation of the Class Handicraft
//  Created on:      06-Nov-2013 1:03:13 AM
//  Original author: JPMMaia
///////////////////////////////////////////////////////////

#if !defined(Handicraft_H__INCLUDED)
#define Handicraft_H__INCLUDED

#include "Piece.h"

class Handicraft : public Piece
{

public:
	/**
	* Constructor that creates a default Handicraft
	*
	*/
	Handicraft();

	/**
	* Destructor that eliminates the space occupied by an Handicraft
	*
	*/
	~Handicraft();

	/**
	* Function in charge of intializing the class with the correct information, calling LoadData()
	*
	*@param filename which contains the saved data of the object
	*@return returns returns true or false whether the class was succesfully intialized
	*/
	bool Initialize(const std::string& filename);

	/**
	* Function in charge of saving the class's information, calling SaveData()
	*
	*/
	void Shutdown();

	/**
	* Function that tells us what type of piece we're dealing with
	*
	*@return returns a string "Handicraft", indicating we're dealing with an Handicraft
	*/
	std::string GetType() const;

	/**
	* Pure virtual function that returns the id of the piece depending on it's type
	*
	*@return returns the integer corresponding to the piece's type ID
	*/
	unsigned int GetTypeID() const;

private:
	unsigned int mHandicraftID;
	static unsigned int msNumberHandicrafts;
};
#endif // !defined(Handicraft_H__INCLUDED)
